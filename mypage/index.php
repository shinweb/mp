<?php
include_once '../_app/http/mypage/index.php';
appConfig::$title = 'マイページ';
appConfig::$css = <<<EOF
<style>body{background-color:#eee;}</style>
EOF;
include_once '../_template/admin/head.php';
include_once '../_template/admin/l_header.php';
?>

<p class="text-center">登録業者様がログイン後入れるページ</p>

<?php
include_once '../_template/l_footer.php';
?>