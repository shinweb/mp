<?php
include_once '../../_app/http/mypage/login/index.php';
appConfig::$tmpl = 'login';
appConfig::$title = 'ログインページ';
appConfig::$css = <<<EOF
<style>body{background-color:#eee;}</style>
EOF;
include_once '../../_template/admin/head.php';
include_once '../../_template/admin/l_header.php';
?>

<?php
$title  = 'ユーザーログイン';
$errMsg = appHttpMypageLoginIndex::$errMsg;
$appendHtml = '<p class="pt-3 text-center"><a href="/remind/remind/">パスワードをお忘れの場合はこちら</a></p>';
include_once '../../_module/form_login.php';
?>

<div class="container pt-5">
    <?php
    foreach (appConfig::loginCompany as $value) {
        echo 'ID：' . $value["mail"] . '／PASS：' . $value["pass"] . '<br>';
    }
    ?>
</div>

<?php
include_once '../../_template/l_footer.php';
?>