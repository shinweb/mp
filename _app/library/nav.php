<?php
class appLibraryNav
{
    //ページ下部のサイトマップを描画
    public static function sitemap($appConfigSiteMap, $categoryName, $categoryDisp = true)
    {
        $result = "";
        $count = 0;
        foreach ($appConfigSiteMap as $value) {
            $id = 'sec-sitemap-' . $categoryName;
            $link = $value['path'];
            $title = $value['name'];
            if ($categoryName === $value['category'] && $value['categorytop'] === 1 && $categoryDisp === true) {
                $result .= <<<EOF
                <h2 id="{$id}-head" class="border-bottom border-md-none">
                <span class="d-none d-md-block font-size-middle-l font-weight-bold">{$title}</span>
                <button data-toggle="collapse" data-target="#{$id}-body" aria-expanded="true" aria-controls="{$id}-body" class="btn btn-accordion collapsed d-md-none">{$title}</button>
                </h2>
                <div id="{$id}-body" class="collapse d-md-block pdt-middle pdb-large" data-parent="#{$id}-head">
                EOF;
            }
            if ($categoryName === $value['category']) {
                //ページが下層ページの場合
                $result .= '<a href="' . $link . '" class="color-dgray icon-arrow-left d-block pdb-xsmall">' . $title . '</a>';
            }
            $count++;
        }
        if ($categoryDisp === true) {
            $result .= '</div>';
        }
        echo $result;
    }

    //スマートフォン用のメニューを描画
    public static function menu_smt($appConfigSiteMap, $categoryId)
    {
        $result = "";
        foreach ($appConfigSiteMap as $value) {
            $link = $value['path'];
            $pagetitle = $value['name'];
            $price = "";
            $priceClass = 'class="color-highlight font-weight-bold font-size-large pr-2"';
            if ($value['categorytop'] === 1) {
                //ページがカテゴリトップの場合
                $result .= '<a href="' . $link . '" class="link_text link_text-l link_text-lgray font-weight-bold bg-lgray">' . $pagetitle . '</a>';
            } else {
                //ページが下層ページの場合
                //ページが葬儀プランの場合は価格も表記
                switch ($value['path']) {
                    case '/plan/simple':
                        if (appConfig::planItem['plan1']['disp'] === 1) {
                            $price = '<br><span ' . $priceClass . '>' . appConfig::planItem['plan1']['price_orig'] . '</span>円<span class="font-size-small">（税別）</span>';
                            $price .= '<span class="font-size-small d-block">' . appConfig::planItem['plan1']['price'] . '円（税込）</span>';
                        }
                        break;
                    case '/plan/kasou':
                        $price = '<br><span ' . $priceClass . '>' . appConfig::planItem['plan2']['price_orig'] . '</span>円<span class="font-size-small">（税別）</span>';
                        $price .= '<span class="font-size-small d-block">' . appConfig::planItem['plan2']['price'] . '円（税込）</span>';
                        break;
                    case '/plan/ichinichisou':
                        $price = '<br><span ' . $priceClass . '>' . appConfig::planItem['plan3']['price_orig'] . '</span>円<span class="font-size-small">（税別）</span>';
                        $price .= '<span class="font-size-small d-block">' . appConfig::planItem['plan3']['price'] . '円（税込）</span>';
                        break;
                    case '/plan/kazokusou':
                        $price = '<br><span ' . $priceClass . '>' . appConfig::planItem['plan4']['price_orig'] . '</span>円<span class="font-size-small">（税別）</span>';
                        $price .= '<span class="font-size-small d-block">' . appConfig::planItem['plan4']['price'] . '円（税込）</span>';
                        break;
                    case '/plan/ippansou':
                        $price = '<br><span ' . $priceClass . '>' . appConfig::planItem['plan5']['price_orig'] . '</span>円<span class="font-size-small">（税別）</span>';
                        $price .= '<span class="font-size-small d-block">' . appConfig::planItem['plan5']['price'] . '円（税込）</span>';
                        break;
                }
                $result .= '<a href="' . $link . '" class="link_text link_text-l link_text-white">' . $pagetitle . $price . '</a>';
            }
        }
        echo $result;
    }

    //タブナビゲーションを描画
    public static function tabs($data, $tmpl, $link = "", $gettag = null)
    {
        $result = "";
        $class = "";
        $gettag = intval($gettag);
        if (count($data) <= 0) {
            return "";
        }
        foreach ($data as $value) {
            if ($gettag != null && $gettag === $value) {
                $class = "bg-llgray";
            }
            $result .= '<span class="badge badge-base pd-small font-size-middle mr-2 ' . $class . '"><a href="' . $link . '?tags=' . $value . '" class="color-dgray">' . $tmpl[$value]['name'] . '</a></span>';
        }
        echo $result;
    }

    //ページングを描画
    public static function pager($totalElementsCount, $pageInMaxPagerCount)
    {
        $result = ""; //戻り値
        $count = 0; //汎用で使う変数
        $queryString = "?"; //クエリパラメータ
        $pageNum = 1; //ページネーションの初期値
        $pageNum_back = 1;
        $pageNum_next = 0;
        //$pageInMaxPagerCount pagerの数
        $pageRange = 0; // $pageから前後に表示するページ番号の数
        $queryString = "?"; //クエリパラメータ（文字列）
        $queryString_pageNum = ""; //クエリパラメータ（ページ番号）
        $queryString_pageNum_back = ""; //クエリパラメータ（前）
        $queryString_pageNum_next = ""; //クエリパラメータ（次）
        $totalPagerCount = $totalElementsCount / $pageInMaxPagerCount; //ページネーションの要素数
        if (is_float($totalPagerCount)) {
            $totalPagerCount = ceil($totalPagerCount);
        }
        $stylesheet = "";
        $pager_left = "";
        $pager_center = "";
        $pager_right = "";
        $currentPageNum = 1;

        //現在のページ番号を取得
        if (isset($_GET["page"])) {
            $currentPageNum = preg_replace('/[^0-9]/', '', $_GET["page"]);
        }
        //ページネーションの開始数値を設定
        $pageNum = $currentPageNum - 2;
        if ($pageNum <= 0 || $totalPagerCount < $pageInMaxPagerCount) {
            $pageNum = 1;
        } else if ($pageNum >= $totalPagerCount - $pageInMaxPagerCount && $totalPagerCount > $pageInMaxPagerCount) {
            $pageNum = $totalPagerCount - $pageInMaxPagerCount + 1;
        }

        //クエリパラメータ取得
        parse_str(parse_url(appConfig::$uri, PHP_URL_QUERY), $query_array);
        foreach ($query_array as $key => $value) {
            if ($key === 'page') {
                continue;
            }
            if ($value === end($query_array)) {
                $queryString .=   $key . '=' . $value;
            } else {
                $queryString .=   $key . '=' . $value . '&';
            }
        }
        //繰り返し処理：$pager_center作成
        for ($i = 0; $i < $pageInMaxPagerCount; $i++) {
            //分岐1：現在選択中のページか否か（選択中のページにはCSS適用）
            if ($currentPageNum == $pageNum) {
                //処理：CSS適用
                $stylesheet = 'active';
            } else {
                //処理：適用しない
                $stylesheet = 'd-none d-md-block';
            }
            //クエリパラメータにページ番号追加
            $queryString_pageNum = '&page=' . $pageNum;
            //HTML描画
            $pager_center .= '<li class="page-item ' . $stylesheet . '">';
            $pager_center .= '<a class="page-link color-dgray" href="' . $queryString . $queryString_pageNum . '">' . $pageNum . '</a>';
            $pager_center .= '</li>';
            //次のページ番号設定
            $pageNum += 1;
            //ページ番号が$totalPagerCountを超えたら描画終了
            if ($pageNum > $totalPagerCount) {
                break;
            }
        }
        //$pager_left 作成
        if ($currentPageNum != 1) {
            $pageNum_back = $currentPageNum - 1;
        }
        $queryString_pageNum_back = '&page=' . $pageNum_back;
        if ($currentPageNum != 1) {
            $pager_left .= '<li class="page-item">';
            $pager_left .= '<a class="page-link color-dgray" href="' . $queryString . $queryString_pageNum_back . '">';
        } else {
            $pager_left .= '<li class="page-item disabled">';
            $pager_left .= '<span class="page-link">';
        }
        $pager_left .= '<i class="fa fa-caret-left" aria-hidden="true"></i>';
        if ($currentPageNum != 1) {
            $pager_left .= '</a></li>';
        } else {
            $pager_left .= '</span></li>';
        }
        //$pager_right 作成
        if ($currentPageNum < $totalPagerCount) {
            $pageNum_next = $currentPageNum + 1;
        }
        $queryString_pageNum_next = '&page=' . $pageNum_next;
        if ($currentPageNum < $totalPagerCount) {
            $pager_right .= '<li class="page-item">';
            $pager_right .= '<a class="page-link color-dgray" href="' . $queryString . $queryString_pageNum_next . '">';
        } else {
            $pager_right .= '<li class="page-item disabled">';
            $pager_right .= '<span class="page-link">';
        }
        $pager_right .= '<i class="fa fa-caret-right" aria-hidden="true"></i>';
        if ($currentPageNum < $totalPagerCount) {
            $pager_right .= '</a></li>';
        } else {
            $pager_right .= '</span></li>';
        }
        //$HTML作成
        if ($totalPagerCount > 1) {
            $result = <<<EOF
            <nav aria-label="Page navigation example" class="pdt-3 pdb-3">
            <ul class="pagination justify-content-center color-gray mx-auto">
            {$pager_left}
            {$pager_center}
            {$pager_right}
            </ul>
            </nav>
            EOF;
        } else {
            $result = '<!--PAGER-->';
        }
        echo $result;
    }
}
