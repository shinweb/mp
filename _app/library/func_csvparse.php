<?php
//-----------------------------------------------------
// 関数
//-----------------------------------------------------
class appFuncCsvParse
{
    /*
    CSVファイルをパース
     */
    public static function csvparse($csvfile)
    {
        {
            // ファイルの中身を配列で取得.
            $csv = file($csvfile);
            // ヘッダーを切り取る.
            $csv_header = $csv[0];
            $csv_body = array_splice($csv, 1);
            // 各行を配列に直す.
            $array = array();
            $count = 0;
            foreach ($csv_body as $row) {
                $array[$count] = explode(',', $row);
                $count++;
            }
            return $array;
        }
    }
}
