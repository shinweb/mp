<?php
class appLibraryLogin
{
    /*リダイレクト*/
    public function getRedirectPath($arrayLoginDir, $redirectDir, $loginDir)
    {
        $result = null;
        foreach ($arrayLoginDir as $value) {
            //分岐：ディレクトリがリダイレクト対象か？
            if ($redirectDir === $value['dir']) {
                //分岐：サブディレクトリがログインディレクトリか？
                if ($loginDir != $value['subdir']) {
                    $result = '/' . $value['dir'] . '/' . $value['subdir'];
                }
            }
        }
        return $result;
    }
    /*ログインフォームの内容を確認する*/
    public function checkLoginForm($loginUser, $postId = null, $postPassword = null)
    {
        $errorCode = 0;
        $count = 0;
        //ログインID確認
        if ($postId === '' || $postPassword === '') {
            $errorCode = 1;
            return array("errorCode" => $errorCode);
        }
        //ログインID確認
        foreach ($loginUser as $value) {
            if ($postId === $value["mail"] && $postPassword === $value["pass"]) {
                $username = $value["name"];
                return array("errorCode" => $errorCode, "userName" => $username);
                break;
            }
            $count++;
        }
        if ($count == count($loginUser)) {
            $errorCode = 2;
            return array("errorCode" => $errorCode);
        }
    }
}
