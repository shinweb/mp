<?php
class appLibraryForm
{
    //入力フォーム(テキスト)
    public static function text($title, $name, $value, $addText = null, $parm = array())
    {
        $required = "";
        $badge = false;
        $inputtype = "text";
        $addParm = "";
        $addCol = false;
        if ($addText != null && $addText != "") {
            $addCol = true;
        }
        foreach ($parm as $key => $parmValue) {
            switch ($key) {
                case 'size':
                    if ($parmValue === 'small') {
                        $addCol = true;
                    }
                case 'inputtype':
                    $inputtype = $parmValue;
                    break;
                case 'readonly':
                    if ($parmValue === true) {
                        $addParm .= ' readonly';
                    }
                    break;
                case 'required':
                    if ($parmValue === true) {
                        $badge = true;
                        $addParm .= ' required="required"';
                    }
                    break;
            }
        }
        include __DIR__ . '/../../admin/_module/form_input.php';
    }
    //入力フォーム（DATE）
    public static function date($title, $name, $value, $required = 0)
    {
        /*入力必須項目の表記・制御*/
        $badge = "";
        if ($required == 1) {
            $required = 'required="required"';
            $badge = '<span class="badge badge-danger ml-2">必須</span>';
        }
        $name1 = $name . '1';
        /*年月日*/
        $date = "";
        $dateDisp = "";
        if (isset($value[0])) {
            $date = $value[0];
            $dateDisp = date('Y年m月d日', strtotime($date));
        }
        /*時*/
        $name2 = $name . '2';
        $timeH = "";
        if (isset($value[1])) {
            $timeH = $value[1];
        }
        /*分*/
        $name3 = $name . '3';
        $timeI = "";
        if (isset($value[2])) {
            $timeI = $value[2];
        }
        $result = '<div class="row align-items-center pb-2 pt-2 border-bottom">';
        $result .= '<div class="col-4">' . $title . $badge . '</div>';
        $result .= '<div class="col-auto"><span data-toggle>' . $dateDisp . '</span>';
        $result .= '<input type="date" class="form-control d-none" name="' . $name1 . '" value="' . $date . '" data-toggle>';
        $result .= '</div>';
        $result .= '<div class="col-auto"><span data-toggle>' . $timeH . ' </span>';
        $result .= '<input type="number" min="0" max="23" class="form-control d-none" name="' . $name2 . '" value="' . $timeH . '" placeholder="0" data-toggle>';
        $result .= '</div>';
        $result .= '<div class="col-auto">時</div>';
        $result .= '<div class="col-auto"><span data-toggle>' . $timeI . ' </span>';
        $result .= '<input type="number" min="0" max="59" class="form-control d-none" name="' . $name3 . '" value="' . $timeI . '" placeholder="0" data-toggle>';
        $result .= '</div>';
        $result .= '<div class="col-auto">分</div>';
        $result .= '</div>';
        return $result;
    }
    //テキストエリア
    public static function textarea($title, $name, $value)
    {
        $result = '<div class="pb-2">';
        $result .= '<h3 class="pt-3 pb-2 h5">' . $title . '</h3>';
        $result .= '<p class="border-bottom pb-2" data-toggle>' . $value . '</p>';
        $result .= '<textarea class="form-control d-none" name="' . $name . '" rows="3" data-toggle>' . $value . '</textarea>';
        $result .= '</div>';
        return $result;
    }
    //チェックボックス(汎用)
    public static function checkbox_def($name, $get, $array)
    {
        $count = 1;
        $result = "";
        foreach ($array as $arrayVal) {
            $checked = "";
            $result .= '<div class="form-check form-check-inline">';
            foreach ($get as $getVal) {
                if ($arrayVal['id'] == $getVal) {
                    $checked = "checked";
                    break;
                }
            }
            $result .= '<input type="checkbox" class="form-check-input" id="form-check-' . $name . '-' . $count . '" name="' . $name . '[]" value="' . $arrayVal['id'] . '" ' . $checked . '>';
            $result .= '<label class="form-check-label mr-2" for="form-check-' . $name . '-' . $count . '">' . $arrayVal['name'] . '</label>';
            $result .= '</div>';
            $count++;
        }
        return $result;
    }
    //ラジオボタン(汎用)
    public static function radio($name, $value, $array)
    {
        $count = 1;
        $result = "";
        $value = strval($value);
        foreach ($array as $arrayVal) {
            $checked = "";
            $id = strval($arrayVal['id']);
            $result .= "\n";
            $result .= '<div class="form-check form-check-inline">';
            if ($value == $id) {
                $checked = "checked";
            }
            $result .= '<input type="radio" class="form-check-input" id="form-radio-' . $name . '-' . $count . '" name="' . $name . '" value="' . $arrayVal['id'] . '" ' . $checked . '>';
            $result .= '<label class="form-check-label mr-2" for="form-radio-' . $name . '-' . $count . '">' . $arrayVal['name'] . '</label>';
            $result .= '</div>';
            $count++;
        }
        return $result;
    }
    //ラジオボタン（管理画面用）
    public static function radio_editPage($title, $name, $value, $array)
    {
        $result = '<div class="row align-items-center pb-2 pt-2 border-bottom">';
        $result .= '<div class="col-4">' .  $title . '</div>';
        if ($value == "") {
            $value = 0;
        }
        $result .= '<div class="col-8"><span data-toggle>' . $array[$value]['name']  . '</span>';
        $result .= '<div class="d-none" data-toggle>';
        $count = 1;
        foreach ($array as $arrayVal) {
            $id = $arrayVal['id'];
            $checked = "";
            $result .= '<div class="form-check form-check-inline">';
            if ($value == $id) {
                $checked = "checked";
            }
            $result .= '<input type="radio" class="form-check-input" id="form-radio-' . $name . '-' . $count . '" name="' . $name . '" value="' . $id . '" ' . $checked . '>';
            $result .= '<label class="form-check-label mr-2" for="form-radio-' . $name . '-' . $count . '">' . $arrayVal['name'] . '</label>';
            $result .= '</div>';
            $count++;
        }
        $result .= '</div>';
        $result .= '</div>';
        $result .= '</div>';
        return $result;
    }
    //セレクトメニュー（汎用）
    public static function selectmenu_def($name, $value, $array)
    {
        $value = strval($value);
        $result = "";
        $result .= '<select class="form-control" name="' . $name . '">';
        foreach ($array as $arrayVal) {
            if ($value === $arrayVal['id']) {
                $result .= '<option value="' . $arrayVal['id'] . '" selected>' . $arrayVal['name'] . '</option>';
            } else {
                $result .= '<option value="' . $arrayVal['id'] . '">' . $arrayVal['name'] . '</option>';
            }
        }
        $result .= '</select>';
        return $result;
    }
    //セレクトメニュー
    public static function selectmenu($title, $name, $value, $array)
    {
        if ($value == "") {
            $value = 0;
        }
        $result = '<div class="row align-items-center pb-2 pt-2 border-bottom">';
        $result .= '<div class="col-4">' . $title . '</div>';
        $result .= '<div class="col-8"><span data-toggle>' . $array[$value]['name'] . '</span>';
        $result .= '<select class="form-control d-none" name="' . $name . '" data-toggle>';
        foreach ($array as $arrayVal) {
            if ($value == $arrayVal['id']) {
                $result .= '<option value="' . $arrayVal['id'] . '" selected>' . $arrayVal['name'] . '</option>';
            } else {
                $result .= '<option value="' . $arrayVal['id'] . '">' . $arrayVal['name'] . '</option>';
            }
        }
        $result .= '</select></div></div>';
        return $result;
    }
    //セレクトメニュー都道府県
    public static function selectmenu_area($name, $array, $get_area = null)
    {
        $body = '<select class="form-control" name="' . $name . '" data-select-area>';
        $body .= '<option>都道府県を選択してください</option>';
        foreach ($array as $value) {
            if ($get_area == $value) {
                $body .= '<option value="' . $value . '" selected>' . $value . '</option>';
            } else {
                $body .= '<option value="' . $value . '">' . $value . '</option>';
            }
        }
        $body .= '</select>';
        return $body;
    }
    //セレクトメニュー市区町村
    public static function selectmenu_city($results, $city = null)
    {
        $body = '<select class="form-control" name="city">';
        $body .= '<option>市区町村を選択してください</option>';
        foreach ($results as $value) {
            if ($city == $value['city']) {
                $body .= '<option value="' . $value['city'] . '" selected>' . $value['city'] . '</option>';
            } else {
                $body .= '<option value="' . $value['city'] . '">' . $value['city'] . '</option>';
            }
        }
        $body .= '</select>';
        return $body;
    }
}
