<?php
//-----------------------------------------------------
// 関数
//-----------------------------------------------------
class appFuncCurrentPage
{
    public static function currentDirectory($uri, $directoryName = '', $int = 1)
    {
        $dir = explode('/', $uri);
        $dirname = $dir[$int];
        if ($dirname === $directoryName) {
            echo 'is-current';
        }
    }
}
