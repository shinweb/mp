<?php
//-----------------------------------------------------
// ファイルの作成・編集・削除関数
//-----------------------------------------------------
class appFuncEditFile
{
    //ファイル生成
    public static function createFile($result, $filepath)
    {
            // ファイルを書き込みモードで開く
            $file_handle = fopen($filepath, "w");
            // ファイルへデータを書き込み
            fwrite($file_handle, $result);
            // ファイルを閉じる
            fclose($file_handle);
    }//appFuncEditFile::createFile();
}
