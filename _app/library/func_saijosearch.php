<?php
class appHttpSaijoSearch
{
    //検索キーワードを表示
    public static function searchWords($get_value)
    {
        $get_words = $_GET[$get_value];
        $get_words = str_replace('　', ' ', $get_words);
        $get_wordsExplode = explode(' ', $get_words);
        $result = "";
        foreach ($get_wordsExplode as $value) {
            if ($value === reset($get_wordsExplode)) {
                $result .= $value;
            } else {
                $result  .= '+' . $value;
            }
        }
        return $result;
    }

    //検索結果を表示
    public static function getData()
    {
        include_once '../_app/library/func_db.php'; //DB操作
        include_once '../_app/database/sql_saijo.php'; //SQLの呼び出し

        $get_pageNum = ""; //ページ番号
        $pageOffset = ""; //選択中のページ番号
        $sql = ""; //SQL
        $sql_where = ""; //where句
        $sql_orderBy = ""; //orderBy句
        $sql_limit = ' limit 10'; //limit値
        $get_words = ""; //検索するキーワード
        $get_wordsExplode = ""; //$get_wordsを配列に変えたもの
        $get_parking = ""; //駐車場の有無 0or1
        $get_anti = ""; //安置の有無 0or1
        $get_kashikiri = ""; //貸し切りの有無 0or1
        $get_sort = ""; //検索するキーワード 0or1
        $get_searchtype = "all"; //検索の種類。デフォルトは"all"（全部表示）。
        $str_searchWords = ""; //検索キーワードを閲覧者用にカスタマイズしたもの。
        $params = array(); //PDOで使用する変数

        //======================================================================
        // 1.SQLを選定
        //======================================================================
        if (isset($_GET["searchtype"])) {
            $get_searchtype = $_GET["searchtype"];
        }
        $get_words = htmlspecialchars($_GET["words"], ENT_QUOTES, "UTF-8");
        $get_words = str_replace('　', ' ', $get_words);
        $get_wordsExplode = explode(' ', $get_words);
        //分岐1：キーワード検索
        if ($get_searchtype === "keyword") {
            $sql = sqlSaijo::$searchKeyWord . " and ";
            foreach ($get_wordsExplode as $value) {
                $sql_where .= '(';
                $sql_where .=  'name LIKE "%' . $value . '%" or ';
                $sql_where .=  'address LIKE "%' . $value . '%" or ';
                $sql_where .=  'comment LIKE "%' . $value . '%" or ';
                $sql_where .=  'access LIKE "%' . $value . '%"';
                if ($value === end($get_wordsExplode)) {
                    $sql_where .= ')';
                } else {
                    $sql_where .= ') and ';
                }
            }
        }
        //分岐2：地域で検索
        if ($get_searchtype === "area") {
            $sql = sqlSaijo::$searchArea;
            $params[':area'] = $area_name;
            $params[':city'] = $city_name;
            $params[':area_sub'] = '%' . $area_name . '%';
            $params[':city_sub'] = '%' . $city_name . '%';
        }
        //分岐3：駅・路線検索
        if ($get_searchtype === "station") {
            $sql = sqlSaijo::$searchStation;
            $params[':words'] = '%' . $get_words . '%';
        }
        //分岐4：全表示
        if ($get_searchtype === "all") {
            $sql = sqlSaijo::$searchAll;
        }
        //======================================================================
        // 2.絞り込み・ソートを行うWhere句を追加
        //======================================================================
        //ページ番号：$_GET["page"]
        if (isset($_GET["page"]) && is_numeric($_GET["page"])) {
            $get_pageNum = intval($_GET["page"]);
            $pageOffset = ($get_pageNum - 1) * 10;
            $sql_limit = ' limit 10 offset ' . $pageOffset;
        }
        //絞込：駐車場の有無：$_GET["parking"]
        if (isset($_GET["parking"]) && is_numeric($_GET["parking"])) {
            $get_parking = intval($_GET["parking"]);
            $sql_where .= ' and parking>=:parking';
            $params[':parking'] = $get_parking;
        }
        //絞込：安置の可否：$_GET["anti"]
        if (isset($_GET["anti"]) && is_numeric($_GET["anti"])) {
            $get_anti = intval($_GET["anti"]);
            $sql_where .= ' and anti>=:anti';
            $params[':anti'] = $get_anti;
        }
        //絞込：貸切の可否：$_GET["kashikiri"]
        if (isset($_GET["kashikiri"]) && is_numeric($_GET["kashikiri"])) {
            $get_kashikiri = intval($_GET["kashikiri"]);
            $sql_where .= ' and kashikiri>=:kashikiri';
            $params[':kashikiri'] = $get_kashikiri;
        }
        //======================================================================
        // 3.ORDER BY句を追加
        //======================================================================
        //ソートする基準：$_GET["sort"]
        if (isset($_GET["sort"]) && strlen($_GET["sort"]) < 20) {
            $get_sort = $_GET["sort"];
            switch ($get_sort) {
                case 'osusume':
                    $sql_orderBy .= ' ORDER BY osusume desc';
                    break;
                case 'kibo_l':
                    $sql_orderBy .= ' ORDER BY kibo_max desc';
                    break;
                case 'price_l':
                    $sql_orderBy .= ' ORDER BY price desc';
                    break;
                case 'kibo_s':
                    $sql_orderBy .= ' ORDER BY kibo_min';
                    break;
            }
        }

        $dbh = appFuncDatabase::connect();
        $sql = $sql . $sql_where . $sql_orderBy . $sql_limit;
        $results = appFuncDatabase::getData($dbh, $sql, $params);
        return $results;
    }
}
