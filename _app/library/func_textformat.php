<?php
//-----------------------------------------------------
// 関数
//-----------------------------------------------------
class appFuncTextformat
{
    /*
    テスト用
    */
    public function test()
    {
        return "appFunc";
    }
    /*
    文字列に値やタグをねじ込むときに使用。
    使い方：format_textAdd('文字列','何文字目に要素を挿入するか？','挿入したい要素')
     */
    public static function format_textAdd($str, $val, $add)
    {
        $str1 = mb_substr($str, 0, $val);
        $str2 = mb_substr($str, $val);
        $result = $str1 . $add . $str2;
        return $result;
    }
    /*
    濁点の除去
    50音順の一覧表など作るときに使用
    */
    public static function delDakuten($zenkakuHiragana)
    {
        $encode = "utf-8";
        $hankakuKatakana = mb_convert_kana($zenkakuHiragana, "h", $encode);
        $zenkakuHiragana = mb_convert_kana($hankakuKatakana, "H", $encode);
        if (mb_strlen($zenkakuHiragana, $encode) > 1) {
            $zenkakuHiragana = mb_substr($zenkakuHiragana, 0, 1, $encode);
        }
        return $zenkakuHiragana;
    }
    /*
    時刻のフォーマット
    */
    public static function datetime($datetime, $type = "datetime")
    {
        if ($type === "datetime") {
            $result = date('Y年m月d日 H:i', strtotime($datetime));
        } else  if ($type === "date") {
            $result = date('Y年m月d日', strtotime($datetime));
        } else {
            $result = $datetime;
        }
        return $result;
    }
    /*
    テキスト置換
    */
    public static function str_replace($search, $replace, $subject)
    {
        $result = str_replace($search, $replace, $subject);
        return $result;
    }
    /*
    テキスト置換
    */
    public static function searchReplaceEcho($search, $replace, $subject)
    {
        $pos = strpos($subject, $search);
        if ($pos === false) {
            $result = "";
        } else {
            $result = str_replace($search, $replace, $subject);
        }
        return $result;
    }
    /*
    文字数を数える
    */
    public static function textCount($string): int
    {
        $stringAdjust = strip_tags($string);
        $stringAdjust = trim($stringAdjust);
        $count = mb_strlen($stringAdjust);
        return $count;
    }
}

//タイマー用の時刻設定
date_default_timezone_set('Asia/Tokyo');
