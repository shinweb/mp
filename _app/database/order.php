<?php
/*======================================================================

注文情報

======================================================================*/
class appDatabaseOrder
{
    public static $count;
    public static $list;
    public static $detail;
    public static $insert;
    public static $update;
    public static $latest;
    public static $searchWords;
    public static $enquete;
}

appDatabaseOrder::$count = <<<EOF
SELECT
COUNT(*) as count
FROM
order_list
WHERE
EOF;

appDatabaseOrder::$list = <<<EOF
SELECT
id,
repeater,
client_id,
plan_id,
company_id,
saijo_id,
kasou_id,
day_sougi,
day_sekou,
status,
kojin_name,
kojin_kana_name,
int_ninzu,
comment,
insert_date,
update_date,
update_by
FROM
order_list
WHERE
EOF;

appDatabaseOrder::$detail = <<<EOF
SELECT
id,
repeater,
client_id,
plan_id,
company_id,
saijo_id,
kasou_id,
day_sougi,
day_sekou,
status,
kojin_name,
kojin_kana_name,
kojin_address,
int_ninzu,
option_anti,
option_obousan,
option_bodaiji,
option_frower_hitsugi,
option_frower_kyouka,
option_food_tuya,
option_food_shoujin,
option_henreihin,
option_yukan,
comment,
token,
insert_date,
update_date,
update_by
FROM
order_list
WHERE
id=:id
EOF;

appDatabaseOrder::$insert = <<<EOF
INSERT INTO `order_list` (
    id,
    repeater,
    client_id,
    plan_id,
    company_id,
    saijo_id,
    kasou_id,
    day_sougi,
    day_sekou,
    status,
    kojin_name,
    kojin_kana_name,
    kojin_address,
    int_ninzu,
    option_anti,
    option_obousan,
    option_bodaiji,
    option_frower_hitsugi,
    option_frower_kyouka,
    option_food_tuya,
    option_food_shoujin,
    option_henreihin,
    option_yukan,
    comment,
    insert_date,
    update_date,
    update_by
) VALUES (
    :id,
    :repeater,
    :client_id,
    :plan_id,
    :company_id,
    :saijo_id,
    :kasou_id,
    :day_sougi,
    :day_sekou,
    :status,
    :kojin_name,
    :kojin_kana_name,
    :kojin_address,
    :int_ninzu,
    :option_anti,
    :option_obousan,
    :option_bodaiji,
    :option_frower_hitsugi,
    :option_frower_kyouka,
    :option_food_tuya,
    :option_food_shoujin,
    :option_henreihin,
    :option_yukan,
    :comment,
    :insert_date,
    :update_date,
    :update_by
);
EOF;

appDatabaseOrder::$update = <<<EOF
UPDATE order_list
SET
client_id=:client_id,
repeater=:repeater,
plan_id=:plan_id,
company_id=:company_id,
saijo_id=:saijo_id,
kasou_id=:kasou_id,
day_sougi=:day_sougi,
day_sekou=:day_sekou,
status=:status,
kojin_name=:kojin_name,
kojin_kana_name=:kojin_kana_name,
kojin_address=:kojin_address,
int_ninzu=:int_ninzu,
option_anti=:option_anti,
option_obousan=:option_obousan,
option_bodaiji=:option_bodaiji,
option_frower_hitsugi=:option_frower_hitsugi,
option_frower_kyouka=:option_frower_kyouka,
option_food_tuya=:option_food_tuya,
option_food_shoujin=:option_food_shoujin,
option_henreihin=:option_henreihin,
option_yukan=:option_yukan,
comment=:comment,
update_date=:update_date,
update_by=:update_by
WHERE
id = :id
EOF;

appDatabaseOrder::$latest = <<<EOF
SELECT
id
FROM
order_list
order by id desc
limit 1
EOF;

appDatabaseOrder::$searchWords = <<<EOF
SELECT
o.id as id,
o.plan_id as plan_id,
o.client_id as client_id,
o.company_id,
o.status as status,
o.day_sougi as day_sougi,
o.saijo_id,
o.kasou_id,
o.kojin_name,
o.kojin_kana_name,
o.kojin_address,
o.comment,
o.insert_date as insert_date,
o.update_date as update_date,
cl.id as clid,
cl.name,
cl.name_kana,
cl.tel,
cl.address,
cl.mail,
cp.id as cpid,
cp.name,
cp.tel,
cp.address,
cp.comment
FROM
order_list AS o
LEFT OUTER JOIN client AS cl ON cl.id = o.client_id
LEFT OUTER JOIN company AS cp ON cp.id = o.company_id
WHERE
EOF;

appDatabaseOrder::$latest = <<<EOF
SELECT
id
FROM
order_list
order by id desc
limit 1
EOF;
