<?php
/*======================================================================

顧客情報

======================================================================*/
class appDatabaseClient
{
    public static $list;
    public static $detail;
    public static $insert;
    public static $update;
    public static $latest;
}

appDatabaseClient::$list = <<<EOF
SELECT
id,
name,
name_kana,
tel,
address,
mail,
comment,
insert_date,
update_date,
update_by
FROM
client
EOF;

appDatabaseClient::$detail = <<<EOF
SELECT
id,
name,
name_kana,
tel,
address,
mail,
comment,
insert_date,
update_date,
update_by
FROM
client
WHERE
id=:id
EOF;

appDatabaseClient::$insert = <<<EOF
INSERT INTO `client` (
    id,
    name,
    name_kana,
    tel,
    address,
    mail,
    insert_date,
    update_date,
    update_by
) VALUES (
    :id,
    :name,
    :name_kana,
    :tel,
    :address,
    :mail,
    :insert_date,
    :update_date,
    :update_by
);
EOF;

appDatabaseClient::$update = <<<EOF
UPDATE client
SET
name=:name,
name_kana=:name_kana,
tel=:tel,
address=:address,
mail=:mail,
update_date=:update_date,
update_by=:update_by
WHERE
id = :id
EOF;

appDatabaseClient::$latest = <<<EOF
SELECT
id
FROM
client
order by id desc
limit 1
EOF;
