<?php
/*======================================================================

アンケート

======================================================================*/
class appDatabaseQuestionnaire
{
    public static $disp;
    public static $insert;
    public static $update;
}

appDatabaseQuestionnaire::$disp = <<<EOF
SELECT
order_id,
question_id,
answer
FROM
questionnaire
WHERE
order_id=:order_id
EOF;

appDatabaseQuestionnaire::$insert = <<<EOF
INSERT INTO `questionnaire` (
    order_id,
    client_id,
    company_id,
    question_id,
    answer,
    comment,
    insert_date,
    update_date,
    update_by
) VALUES (
    :order_id,
    :client_id,
    :company_id,
    :question_id,
    :answer,
    :comment,
    :insert_date,
    :update_date,
    :update_by
);
EOF;

appDatabaseQuestionnaire::$update = <<<EOF
UPDATE questionnaire
SET
order_id=:order_id,
client_id=:client_id,
company_id=:company_id,
question_id=:question_id,
answer=:answer,
comment=:comment,
update_date=:update_date,
update_by=:update_by
WHERE
order_id=:order_id and
question_id=:question_id
EOF;
