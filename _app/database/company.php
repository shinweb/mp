<?php
/*======================================================================

更新情報

======================================================================*/
class appDatabaseCompany
{
    public static $list;
    public static $detail;
    public static $insert;
    public static $update;
}

appDatabaseCompany::$list = <<<EOF
SELECT
id,
name,
address,
tel,
point,
disp
FROM
company
where
disp=1
EOF;

appDatabaseCompany::$detail = <<<EOF
SELECT
id,
name,
address,
tel,
point,
area,
comment,
disp,
insert_date,
update_date
FROM
company
WHERE
id=:id
EOF;


appDatabaseCompany::$insert = <<<EOF
INSERT INTO `news` (
    id, 
    date,
    category,
    title,
    link,
    disp,
    create_date,
    update_date
) VALUES (
    :id, 
    :date,
    :category,
    :title,
    :link,
    :disp,
    :create_date,
    :update_date
);
EOF;

appDatabaseCompany::$update = <<<EOF
UPDATE news
SET
date=:date,
category=:category,
title=:title,
link=:link,
disp=:disp,
update_date=:update_date
WHERE
id = :id
EOF;
