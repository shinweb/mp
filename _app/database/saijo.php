<?php
/*======================================================================

式場検索

======================================================================*/
class sqlSaijo
{
    public static $adminList;
    public static $adminEdit;
    public static $adminInsert;
    public static $adminUpdate;
    public static $adminDelete;

    public static $searchKeyWord;
    public static $searchKeyWord_count;
    public static $searchStation;
    public static $searchStation_count;
    public static $searchArea;
    public static $searchArea_count;
    public static $searchAll;
    public static $searchAll_count;
    public static $osusumeAll;
    public static $detail;
}

sqlSaijo::$adminList = <<<EOF
SELECT
*
FROM
saijo_list2
order by update_date desc
EOF;

sqlSaijo::$adminEdit = <<<EOF
SELECT
*
FROM
saijo_list2
WHERE
id = :id
EOF;

sqlSaijo::$adminInsert = <<<EOF
INSERT INTO `saijo_list2` (
    id,
    title,
    date,
    thumbnail,
    category,
    body,
    create_date,
    update_date,
    disp
) VALUES (
    :id,
    :title,
    :date,
    :thumbnail,
    :category,
    :body,
    :create_date,
    :update_date,
    :disp
);
EOF;

sqlSaijo::$adminUpdate = <<<EOF
UPDATE saijo_list2
SET
title=:title,
date=:date,
thumbnail=:thumbnail,
category=:category,
body=:body,
update_date=:update_date,
disp=:disp
WHERE
id = :id
EOF;


//キーワード検索
sqlSaijo::$searchKeyWord = <<<EOF
SELECT
*
FROM
saijo_list2
WHERE
disp=1
EOF;

//キーワード検索_件数取得
sqlSaijo::$searchKeyWord_count = <<<EOF
SELECT
COUNT(*) as count
FROM
saijo_list2
WHERE
disp=1
EOF;

//駅・路線検索
sqlSaijo::$searchStation = <<<EOF
SELECT
*
FROM
saijo_list2
WHERE
access like :words and
disp=1
EOF;

//駅・路線検索_件数取得
sqlSaijo::$searchStation_count = <<<EOF
SELECT
COUNT(*) as count
FROM
saijo_list2
WHERE
access like :words and
disp=1
EOF;

//地域検索
sqlSaijo::$searchArea = <<<EOF
SELECT
*
FROM
saijo_list2
WHERE
(area = :area or area_sub like :area_sub) and
(city = :city or city_sub like :city_sub) and
disp=1
EOF;

//地域検索_件数取得
sqlSaijo::$searchArea_count = <<<EOF
SELECT
COUNT(id) as count
FROM
saijo_list2
WHERE
(area = :area or area_sub like :area_sub) and
(city = :city or city_sub like :city_sub) and
disp=1
EOF;

//全データ表示
sqlSaijo::$searchAll = <<<EOF
SELECT
*
FROM
saijo_list2
WHERE
disp=1
EOF;

//全データ表示_件数取得
sqlSaijo::$searchAll_count = <<<EOF
SELECT
COUNT(id) as count
FROM
saijo_list2
WHERE
disp=1
EOF;

//全データ表示＞おすすめ取得
sqlSaijo::$osusumeAll = <<<EOF
SELECT
*
FROM
saijo_list2
WHERE
osusume=1 and
disp=1
EOF;

//詳細表示
sqlSaijo::$detail = <<<EOF
SELECT
*
FROM
saijo_list2
WHERE
id=:id and
disp=1
EOF;
