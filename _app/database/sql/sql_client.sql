CREATE TABLE `client` (
  `id` int(6) NOT NULL,
  `name` varchar(40) DEFAULT NULL,
  `name_kana` varchar(40) DEFAULT NULL,
  `tel` varchar(20) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `mail` varchar(100) DEFAULT NULL,
  `comment` varchar(400) DEFAULT NULL,
  `insert_date` date DEFAULT NULL,
  `update_date` date DEFAULT NULL,
  `update_by` varchar(40) DEFAULT NULL
) ENGINE = InnoDB DEFAULT CHARSET = utf8mb4;

ALTER TABLE
  `client`
ADD
  UNIQUE KEY `id` (`id`);
COMMIT;