<?php
/*======================================================================

式場検索

======================================================================*/
class appDatabaseOrder
{
    public static $list;
    public static $detail;
    public static $insert;
    public static $update;
    public static $latest;
}

appDatabaseOrder::$list = <<<EOF
SELECT
id,
name,
status,
plan_id,
day_sougi,
insert_date,
update_date
FROM
orderlist
order by id desc
EOF;

appDatabaseOrder::$detail = <<<EOF
SELECT
id,
name,
name_kana,
tel,
address,
mail,
plan_id,
company_id,
saijo_id,
kasou_id,
day_sougi,
status,
kojin_name,
kojin_kana_name,
kojin_address,
int_ninzu,
option_anti,
option_obousan,
option_bodaiji,
option_frower_hitsugi,
option_frower_kyouka,
option_food_tuya,
option_food_shoujin,
option_henreihin,
option_yukan,
comment,
insert_date,
update_date,
update_by
FROM
orderlist
WHERE
id=:id
EOF;

appDatabaseOrder::$insert = <<<EOF
INSERT INTO `orderlist` (
    id,
    name,
    name_kana,
    tel,
    address,
    mail,
    plan_id,
    company_id,
    saijo_id,
    kasou_id,
    day_sougi,
    status,
    kojin_name,
    kojin_kana_name,
    kojin_address,
    int_ninzu,
    option_anti,
    option_obousan,
    option_bodaiji,
    option_frower_hitsugi,
    option_frower_kyouka,
    option_food_tuya,
    option_food_shoujin,
    option_henreihin,
    option_yukan,
    comment,
    insert_date,
    update_date,
    update_by
) VALUES (
    :id,
    :name,
    :name_kana,
    :tel,
    :address,
    :mail,
    :plan_id,
    :company_id,
    :saijo_id,
    :kasou_id,
    :day_sougi,
    :status,
    :kojin_name,
    :kojin_kana_name,
    :kojin_address,
    :int_ninzu,
    :option_anti,
    :option_obousan,
    :option_bodaiji,
    :option_frower_hitsugi,
    :option_frower_kyouka,
    :option_food_tuya,
    :option_food_shoujin,
    :option_henreihin,
    :option_yukan,
    :comment,
    :insert_date,
    :update_date,
    :update_by
);
EOF;

appDatabaseOrder::$update = <<<EOF
UPDATE orderlist
SET
name=:name,
name_kana=:name_kana,
tel=:tel,
address=:address,
mail=:mail,
plan_id=:plan_id,
company_id=:company_id,
saijo_id=:saijo_id,
kasou_id=:kasou_id,
day_sougi=:day_sougi,
status=:status,
kojin_name=:kojin_name,
kojin_kana_name=:kojin_kana_name,
kojin_address=:kojin_address,
int_ninzu=:int_ninzu,
option_anti=:option_anti,
option_obousan=:option_obousan,
option_bodaiji=:option_bodaiji,
option_frower_hitsugi=:option_frower_hitsugi,
option_frower_kyouka=:option_frower_kyouka,
option_food_tuya=:option_food_tuya,
option_food_shoujin=:option_food_shoujin,
option_henreihin=:option_henreihin,
option_yukan=:option_yukan,
comment=:comment,
update_date=:update_date,
update_by=:update_by
WHERE
id = :id
EOF;

appDatabaseOrder::$latest = <<<EOF
SELECT
id
FROM
orderlist
order by id desc
limit 1
EOF;
