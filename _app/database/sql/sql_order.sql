CREATE TABLE `client` (
    `id` int(6) NOT NULL,
    `client_id` int(6) NOT NULL,
    `plan_id` int(6) NOT NULL,
    `saijo_id` int(6) NOT NULL,
    `kasou_id` int(6) NOT NULL,
    `day_kadou` date DEFAULT NULL,
    `day_sougi` date DEFAULT NULL,
    `status` int(1) DEFAULT 0,
    `kojin_name` varchar(40) DEFAULT NULL,
    `kojin_address` varchar(100) DEFAULT NULL,
    `int_ninzu` int(4) DEFAULT 0,
    `option_anti` int(1) DEFAULT 0,
    `option_obousan` int(1) DEFAULT 0,
    `option_bodaiji` int(1) DEFAULT 0,
    `option_frower_hitsugi` int(2) DEFAULT 0,
    `option_frower_kyouka` int(2) DEFAULT 0,
    `option_food_tuya` int(3) DEFAULT 0,
    `option_food_shoujin` int(3) DEFAULT 0,
    `option_henreihin` int(3) DEFAULT 0,
    `option_yukan` int(3) DEFAULT 0,
    `comment` varchar(100) DEFAULT NULL,
    `insert_date` date DEFAULT NULL,
    `update_date` date DEFAULT NULL,
    `update_by` varchar(40) DEFAULT NULL
) ENGINE = InnoDB DEFAULT CHARSET = utf8mb4;

ALTER TABLE `client`
  ADD UNIQUE KEY `id` (`id`);
COMMIT;