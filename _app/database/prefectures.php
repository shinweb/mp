<?php
/*======================================================================

市区町村

======================================================================*/
class appDatabasePrefectures
{
    public static $citylist;
    public static $citydetail;
    public static $latest;
    public static $address;
}

appDatabasePrefectures::$citylist = <<<EOF
SELECT DISTINCT
jis_code,area,city
FROM
prefectures
WHERE
area=:area
order by jis_code asc
EOF;

appDatabasePrefectures::$citydetail = <<<EOF
SELECT
jis_code,area,city
FROM
prefectures
WHERE
EOF;

appDatabasePrefectures::$address = <<<EOF
SELECT DISTINCT
postal_code,area,city
FROM
prefectures
WHERE
postal_code=:postal_code
EOF;