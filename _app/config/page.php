<?php
/*======================================================================

汎用的な文字列や数値を集約させています

======================================================================*/
class appConfigPage
{
    //最終更新日（可変）
    public static $update;
    //ページの名称
    public static $title = "MP";
    //ページの概要
    public static $description = "";
    //ページに適用するテンプレートの種類
    public static $tmpl = "default";
    //ページに適用するオープングラフの画像
    public static $ogimage = "";
    //ページ固有のCSSを適用する時に使用
    public static $css;
    //ページ固有のJavaScriptを適用する時に使用
    public static $js;
    public static $js_footer;
    //ページID（特定の$pageIDを持つページに特殊な処理をさせたい・・・という場面で使用）
    public static $pageID = "default";
    //グローバルメニュー
    public static $gmenu = "ここにメニューが入ります";
    //パンくず拡張用
    public static $breadcrumb = "";
    //URI
    public static $uri = "";
    //ページカテゴリ名
    public static $categoryName = "";
}
