<?php
/*======================================================================

汎用的な文字列や数値を集約させています

======================================================================*/
class appConfig
{
    //最終更新日（固定）
    const update = '20211025';
    //基本となるURL
    const baseUrl = "http://mptest2.prototype-web.net/";
    //ログイン対象ディレクトリ
    const loginDirName = "admin";
    //ウェブサイト名称
    const siteName = "まかせてお葬式";
    //会社の名称
    const companyName = "サービス名";
    //会社の住所
    const companyAddress = "";
    //メールアドレス
    const mailAddres = "test@prototype-web.net"; //本番
    const mailAddres_test = "test@prototype-web.net"; //テスト用
    //電話番号
    const companyTel = "03-5786-0135";
    //都道府県
    const area = array(
        '北海道',
        '青森県',
        '岩手県',
        '宮城県',
        '秋田県',
        '山形県',
        '福島県',
        '茨城県',
        '栃木県',
        '群馬県',
        '埼玉県',
        '千葉県',
        '東京都',
        '神奈川県',
        '新潟県',
        '富山県',
        '石川県',
        '福井県',
        '山梨県',
        '長野県',
        '岐阜県',
        '静岡県',
        '愛知県',
        '三重県',
        '滋賀県',
        '京都府',
        '大阪府',
        '兵庫県',
        '奈良県',
        '和歌山県',
        '鳥取県',
        '島根県',
        '岡山県',
        '広島県',
        '山口県',
        '徳島県',
        '香川県',
        '愛媛県',
        '高知県',
        '福岡県',
        '佐賀県',
        '長崎県',
        '熊本県',
        '大分県',
        '宮崎県',
        '鹿児島県',
        '沖縄県'
    );
    //ログイン認証が発生するディレクトリ
    const loginDir = [
        'admin' => ['dir' => 'admin', 'subdir' => 'login'],
        'mypage' => ['dir' => 'mypage', 'subdir' => 'login']
    ];
    //ログインセッション名
    const loginSession = [
        'admin' => ['status' => 'adminLoginStatus', 'name' => 'adminLoginName'],
        'mypage' => ['status' => 'mypageLoginStatus', 'name' => 'mypageLoginName']
    ];
    //ログインユーザー
    const loginUser = [
        1 => ['id' => '1', 'mail' => 'y-kobayashi@lab-unlimited.com', 'pass' => '0001', 'name' => 'y-kobayashi'],
        2 => ['id' => '2', 'mail' => 't-kobayashi@lab-unlimited.com', 'pass' => '0002', 'name' => 't-kobayashi'],
        3 => ['id' => '3', 'mail' => 'y-asakura@lab-unlimited.com', 'pass' => '0003', 'name' => 'asakura'],
        4 => ['id' => '4', 'mail' => 'yuki-h@lab-unlimited.com', 'pass' => '0004', 'name' => 'yuki-h'],
        5 => ['id' => '5', 'mail' => 'h-miura@lab-unlimited.com', 'pass' => '0005', 'name' => 'h-miura'],
        6 => ['id' => '6', 'mail' => 'guest', 'pass' => '0000', 'name' => 'guest']
    ];
    //ログインユーザー（企業）
    const loginCompany = [
        1 => ['id' => '1', 'mail' => 'test@example.com', 'pass' => '2001', 'name' => 'y-kobayashi'],
        2 => ['id' => '2', 'mail' => 'sample@example.com', 'pass' => '2002', 'name' => 't-kobayashi'],
        3 => ['id' => '6', 'mail' => 'guest', 'pass' => '0000', 'name' => 'guest']
    ];
    //カテゴリ一覧
    const siteCategory =  [
        'admin' => ['id' => 'about', 'title' => 'オペレーター用管理画面'],
        'mypage' => ['id' => 'plan', 'title' => '企業様用管理画面']
    ];

    //ページの名称
    public static $title = "MP";
    //ページの概要
    public static $description = "";
    //ページに適用するテンプレートの種類
    public static $tmpl = "default";
    //ページに適用するオープングラフの画像
    public static $ogimage = self::baseUrl . "/lp/asset/img/ogp.png";
    //ページ固有のCSSを適用する時に使用
    public static $css;
    //ページ固有のJavaScriptを適用する時に使用
    public static $js;
    public static $js_footer;
    //ページID（特定の$pageIDを持つページに特殊な処理をさせたい・・・という場面で使用）
    public static $pageID = "default";
    //グローバルメニュー
    public static $gmenu = "ここにメニューが入ります";
    //パンくず拡張用
    public static $breadcrumb = "";
    //URI
    public static $uri = "";
    //ページカテゴリ名
    public static $path = "";
    public static $directory = "";
    public static $subDirectory = "";
    //ログインユーザー名
    public static $loginUserName = "";
}

appConfig::$path = explode('/', dirname($_SERVER["SCRIPT_NAME"]));
if (isset(appConfig::$path[1])) {
    appConfig::$directory = appConfig::$path[1];
}
if (isset(appConfig::$path[2])) {
    appConfig::$subDirectory = appConfig::$path[2];
}
