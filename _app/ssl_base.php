<?php
session_start();
//汎用的な数値や変数を呼び出す
include_once __DIR__ . '/define.php'; //開発環境、本番環境で値が変わる値（定数）をここで定義
include_once __DIR__ . '/config/base.php'; //汎用的なテキストや数値を呼ぶ
include_once __DIR__ . '/config/plan.php'; //プランに関する情報を呼ぶ
include_once __DIR__ . '/config/form.php'; //プランに関する情報を呼ぶ
include_once __DIR__ . '/library/database.php'; //DBにデータの送受信を行うときに必要
include_once __DIR__ . '/library/array.php'; //DBにデータの送受信を行うときに必要
include_once __DIR__ . '/library/form.php'; //DBにデータの送受信を行うときに必要
include_once __DIR__ . '/library/nav.php'; //DBにデータの送受信を行うときに必要
include_once __DIR__ . '/library/login.php'; //プランに関する情報を呼ぶ

/*ログイン処理*/
class appSslBase
{
    public static $redirectPath = null;
    public static $sessionStatus = "";
    public static $sessionName = "";
}
/*セッションステータスを取得*/
appSslBase::$sessionStatus = appConfig::loginSession[appConfig::$directory]['status'];
appSslBase::$sessionName = appConfig::loginSession[appConfig::$directory]['name'];

/*リダイレクトパスを取得*/
appSslBase::$redirectPath = appLibraryLogin::getRedirectPath(
    appConfig::loginDir,
    appConfig::$directory,
    appConfig::$subDirectory
);
if (appSslBase::$redirectPath != null) {
    //ログインセッションが無ければ、リダイレクト処理を行う
    if (!isset($_SESSION[appSslBase::$sessionStatus])) {
        header('Location:' . appSslBase::$redirectPath);
        exit();
    } else {
        appConfig::$loginUserName = $_SESSION[appSslBase::$sessionName];
    }
    //$_POST["logout"]が送信されたらログアウト処理を行う
    if (isset($_POST["logout"])) {
        unset($_SESSION[appSslBase::$sessionStatus]);
        header('Location:' . appSslBase::$redirectPath);
        exit();
    }
}
