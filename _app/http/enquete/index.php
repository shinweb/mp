<?php
include_once '../_app/base.php';
include_once '../_app/database/order.php';
include_once '../_app/database/questionnaire.php';

class appHttpEnquete
{
    //GETデータ
    public static $getOrderId;
    //注文データ
    public static $order_params = array(); //DBに送信するパラメータ
    public static $order_results; //DBから戻ってきた値
    public static $order_id; //注文ID
    public static $order_clianrtid; //注文ID
    public static $order_token; //tokenデータ
    public static $order_clientId; //顧客ID
    public static $order_companyId; //企業ID
    //アンケートデータ
    public static $questionnaire_params = array(); //DBに送信するパラメータ
    public static $questionnaire_results; //DBから戻ってきた値
    public static $disp;
    public static $dispStatusNoDisp = 'nodisp';
    public static $dispStatusUnSent = 'unsent';
    public static $dispStatusThanks = 'thanks';
    public static $dispStatusSent = 'sent';
    public static $dispStatusTimeLimit = 'timelimit';

    public static $msg = "";
    //アンケートデータ（INSERT)
    public static $insertQuestionnaire_params = array(); //DBに送信するパラメータ
    //FLAG
    public static $flg = 1;
}

/*
NO.1 注文データを検索
*/
appHttpEnquete::$disp = appHttpEnquete::$dispStatusNoDisp;
if (isset($_GET['id'])) {
    appHttpEnquete::$getOrderId = $_GET['id'];
    appHttpEnquete::$order_params[":id"] = appHttpEnquete::$getOrderId;
    appHttpEnquete::$order_results = appFuncDatabase::getData(
        appFuncDatabase::connect(),
        appDatabaseOrder::$detail,
        appHttpEnquete::$order_params
    );
    if (count(appHttpEnquete::$order_results) > 0) {
        appHttpEnquete::$order_id = appHttpEnquete::$order_results[0]['id'];
        appHttpEnquete::$order_token = appHttpEnquete::$order_results[0]['token'];
        appHttpEnquete::$order_clientId = appHttpEnquete::$order_results[0]['client_id'];
        appHttpEnquete::$order_companyId = appHttpEnquete::$order_results[0]['company_id'];
        appHttpEnquete::$flg += 1;
    }
}

/*
NO.3 GET値のトークンデータと、NO.1で取得したトークンデータを比較
*/
if (appHttpEnquete::$flg === 2 && isset($_GET['t'])) {
    if ($_GET['t'] === appHttpEnquete::$order_results[0]['token']) {
        appHttpEnquete::$flg += 1;
    }
}

/*
NO.4 NO.1で取得したIDを基に、アンケートデータを検索
*/
if (appHttpEnquete::$flg === 3) {
    appHttpEnquete::$questionnaire_params[":order_id"] = appHttpEnquete::$order_id;
    appHttpEnquete::$questionnaire_results = appFuncDatabase::getData(
        appFuncDatabase::connect(),
        appDatabaseQuestionnaire::$disp,
        appHttpEnquete::$questionnaire_params
    );
    appHttpEnquete::$flg += 1;
}

/*
NO.5 アンケートの表示・非表示設定（NO.3で検索した結果、アンケートデータが存在しない場合は表示）
*/
if (appHttpEnquete::$flg === 4) {
    if (count(appHttpEnquete::$questionnaire_results) == 0) {
        //未回答
        appHttpEnquete::$disp = appHttpEnquete::$dispStatusUnSent;
        appHttpEnquete::$flg += 1;
    } else {
        //回答済
        appHttpEnquete::$disp = appHttpEnquete::$dispStatusSent;
    }
}

/*
NO.6データ送信準備
*/
if (appHttpEnquete::$flg === 5) {
    if (isset($_POST['order_id'])) {
        if (appHttpEnquete::$order_id != $_POST['order_id']) {
            appHttpEnquete::$msg = 'エラーが発生しました。ページをリロードし、もう一度回答をお願いいたします。';
        } else {
            appHttpEnquete::$flg += 1;
        }
    }
}

/*
NO.7データ送信処理
*/
if (appHttpEnquete::$flg === 6) {
    foreach ($_POST as $key => $value) {
        if ($key != 'order_id') {
            appHttpEnquete::$insertQuestionnaire_params[":order_id"] = appHttpEnquete::$order_id;
            appHttpEnquete::$insertQuestionnaire_params[":client_id"] = appHttpEnquete::$order_clientId;
            appHttpEnquete::$insertQuestionnaire_params[":company_id"]  = appHttpEnquete::$order_companyId;
            appHttpEnquete::$insertQuestionnaire_params[":question_id"] = 'test';
            if ($key === 'comment') {
                appHttpEnquete::$insertQuestionnaire_params[":answer"] = '';
                if ($value === "") {
                    appHttpEnquete::$insertQuestionnaire_params[":comment"] = '記入なし';
                } else {
                    appHttpEnquete::$insertQuestionnaire_params[":comment"] = $value;
                }
            } else {
                appHttpEnquete::$insertQuestionnaire_params[":answer"] = $value;
                appHttpEnquete::$insertQuestionnaire_params[":comment"] = '';
            }
            appHttpEnquete::$insertQuestionnaire_params[":insert_date"] = date("Y-m-d H:i:s");
            appHttpEnquete::$insertQuestionnaire_params[":update_date"] = date("Y-m-d H:i:s");
            appHttpEnquete::$insertQuestionnaire_params[":update_by"] = 'アンケートフォーム';
            appFuncDatabase::insertData(appFuncDatabase::connect(), appDatabaseQuestionnaire::$insert, appHttpEnquete::$insertQuestionnaire_params);
            //var_dump(appHttpEnquete::$insertQuestionnaire_params);
        } //$key != 'order_id
    }
    //送信感謝表示
    appHttpEnquete::$disp = appHttpEnquete::$dispStatusThanks;
}
