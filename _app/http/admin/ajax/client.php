<?php
include_once '../../_app/ssl_base.php';
include_once '../../_app/database/client.php';

class appHttpAdminAjaxClient
{
    public static $params;
    public static $results;
    public static $body;
    public static $addressArray;
}

if (isset($_GET['id'])) {
    appHttpAdminAjaxClient::$params = array(':id' => $_GET['id']);
    appHttpAdminAjaxClient::$results = appFuncDatabase::getData(
        appFuncDatabase::connect(),
        appDatabaseClient::$detail,
        appHttpAdminAjaxClient::$params
    );
    if (count(appHttpAdminAjaxClient::$results) > 0) {
        appHttpAdminAjaxClient::$addressArray = explode(',', appHttpAdminAjaxClient::$results[0]['address']);
        appHttpAdminAjaxClient::$results[0]['address1'] = appHttpAdminAjaxClient::$addressArray[0];
        appHttpAdminAjaxClient::$results[0]['address2'] = appHttpAdminAjaxClient::$addressArray[1];
        appHttpAdminAjaxClient::$results[0]['address3'] = appHttpAdminAjaxClient::$addressArray[2];
        appHttpAdminAjaxClient::$results[0]['address4'] = appHttpAdminAjaxClient::$addressArray[3];
        echo json_encode(appHttpAdminAjaxClient::$results);
    } else {
        echo 'エラー：送信された値$_GET[id]が不正です';
    }
} else {
    echo 'エラー：値が送信されていません';
}
