<?php
include_once '../../_app/ssl_base.php';
include_once '../../_app/database/prefectures.php';

class appHttpAdminAjaxAddress
{
    public static $params;
    public static $results;
}

if (isset($_GET['postal_code'])) {
    appHttpAdminAjaxAddress::$params = array(':postal_code' => $_GET['postal_code']);
    appHttpAdminAjaxAddress::$results = appFuncDatabase::getData(
        appFuncDatabase::connect(),
        appDatabasePrefectures::$address,
        appHttpAdminAjaxAddress::$params
    );
    echo json_encode(appHttpAdminAjaxAddress::$results);
} else {
    echo '値が送信されていないか、不正な値が送信されました';
}
