<?php
include_once '../../_app/ssl_base.php';
include_once '../../_app/database/prefectures.php';

class appHttpAdminAjaxSelectcity
{
    public static $params;
    public static $results;
    public static $body;
    public static $getArea;
    public static $getType = "selectmenu";
}

if (isset($_GET['area'])) {
    appHttpAdminAjaxSelectcity::$getArea = $_GET['area'];
    appHttpAdminAjaxSelectcity::$params = array(':area' => appHttpAdminAjaxSelectcity::$getArea);
    appHttpAdminAjaxSelectcity::$results = appFuncDatabase::getData(
        appFuncDatabase::connect(),
        appDatabasePrefectures::$citylist,
        appHttpAdminAjaxSelectcity::$params
    );
    if (isset($_GET['type'])) {
        appHttpAdminAjaxSelectcity::$getType = $_GET['type'];
    }
    switch (appHttpAdminAjaxSelectcity::$getType) {
        case "selectmenu":
            appHttpAdminAjaxSelectcity::$body = appLibraryForm::selectmenu_city(appHttpAdminAjaxSelectcity::$results);
            break;
        case "string":
            foreach (appHttpAdminAjaxSelectcity::$results as $value) {
                appHttpAdminAjaxSelectcity::$body .= $value['area'] . $value['city'] . ",";
            }
            break;
    }
    echo appHttpAdminAjaxSelectcity::$body;
} else {
    echo '値が送信されていないか、不正な値が送信されました';
}
