<?php
include_once '../../_app/ssl_base.php';
include_once '../../_app/database/company.php';

class appHttpAdminAjaxCompany
{
    public static $params;
    public static $results;
}

if (isset($_GET['id'])) {
    appHttpAdminAjaxCompany::$params = array(':id' => $_GET['id']);
    appHttpAdminAjaxCompany::$results = appFuncDatabase::getData(
        appFuncDatabase::connect(),
        appDatabaseCompany::$detail,
        appHttpAdminAjaxCompany::$params
    );
    echo json_encode(appHttpAdminAjaxCompany::$results);
} else {
    echo '値が送信されていないか、不正な値が送信されました';
}
