<?php
include_once '../../_app/ssl_base.php';
include_once '../../_app/database/prefectures.php';
include_once '../../_app/database/company.php';

class appHttpAdminCompanyIndex
{
    public static $params = array(); //DBに送信するパラメータ
    public static $sql = "";
    public static $results = ""; //DBから戻ってきた「顧客情報」の値が格納される
    public static $selectmenu = null;
    public static $get_area = "";
    public static $get_city = "";

    public static $id = "";
    public static $name = "";
    public static $update = "";
    public static $table = "";
    public static $point = "";

    function list($results)
    {
        $body = "";
        $rank = 1;
        foreach ($results as $value) {
            $body .= '<tr>';
            $body .= '<td class="text-right">' . $rank . ' </td>';
            $body .= '<td><a href="./detail?id=' . $value['id'] . '">' . $value['name'] . '</a>';
            $body .= '<br><small>' . $value['address'] . '<br>' . $value['tel'] . '</small></td>';
            $body .= '</tr>';
            $rank++;
        }
        return $body;
    }
}

if (isset($_GET['posttype'])) {
    switch ($_GET['posttype']) {
        case 'area':
            /*検索結果*/
            if (isset($_GET['area'])) {
                appHttpAdminCompanyIndex::$get_area = $_GET['area'];
            }
            if (isset($_GET['city'])) {
                appHttpAdminCompanyIndex::$get_city = $_GET['city'];
            }
            appHttpAdminCompanyIndex::$sql = appDatabaseCompany::$list;
            appHttpAdminCompanyIndex::$sql .= ' and area LIKE "%' . appHttpAdminCompanyIndex::$get_area . appHttpAdminCompanyIndex::$get_city . '%"';
            appHttpAdminCompanyIndex::$sql .= ' order by point desc';
            appHttpAdminCompanyIndex::$results = appFuncDatabase::getData(
                appFuncDatabase::connect(),
                appHttpAdminCompanyIndex::$sql,
                appHttpAdminCompanyIndex::$params
            );
            appHttpAdminCompanyIndex::$table = appHttpAdminCompanyIndex::list(appHttpAdminCompanyIndex::$results);

            /*セレクトメニュー*/
            appHttpAdminCompanyIndex::$params = array(':area' => appHttpAdminCompanyIndex::$get_area);
            appHttpAdminCompanyIndex::$results = appFuncDatabase::getData(
                appFuncDatabase::connect(),
                appDatabasePrefectures::$citylist,
                appHttpAdminCompanyIndex::$params
            );
            appHttpAdminCompanyIndex::$selectmenu = appLibraryForm::selectmenu_city(appHttpAdminCompanyIndex::$results, appHttpAdminCompanyIndex::$get_city);
            break;
    }
} else {
}
