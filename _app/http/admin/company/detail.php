<?php
include_once '../../_app/ssl_base.php';
include_once '../../_app/database/company.php';
include_once '../../_app/database/prefectures.php';

class appHttpAdminCompanyDetail
{
    public static $params = array(); //DBに送信するパラメータ
    public static $results = ""; //DBから戻ってきた「顧客情報」の値が格納される

    public static $params_area = array();
    public static $results_area = "";

    public static $id = "";
    public static $name = "";
    public static $tel = "";
    public static $address = "";
    public static $comment = "";
    public static $area = "";
    public static $point = "";
    public static $insert_date = "";
    public static $update_date = "";

    public static $sqlPrefectures = "";
}

if (isset($_POST["id"]) && preg_match("/^[0-9]+$/", $_POST["id"])) {
    appHttpAdminCompanyDetail::$params = array(
        ':id' => $_POST["id"],
        ':name' => $_POST["name"],
        ':name_kana' =>  $_POST["name_kana"],
        ':address' =>  $_POST["address"],
        ':tel' =>  $_POST["tel"],
        ':mail' =>  $_POST["mail"],
        ':comment' =>  $_POST["comment"],
        ':update_date' => date("Y-m-d H:i:s"),
        ':update_by' =>  'ADMIN'
    );
    appFuncDatabase::updateData(appFuncDatabase::connect(), appDatabaseCompany::$update, appHttpAdminCompanyDetail::$params);
}

if (isset($_GET["id"])) {
    appHttpAdminCompanyDetail::$params = array();
    appHttpAdminCompanyDetail::$id = preg_replace('/[^0-9]/', '',  $_GET["id"]);
    appHttpAdminCompanyDetail::$id = intval(appHttpAdminCompanyDetail::$id);
    appHttpAdminCompanyDetail::$params = array("id" => appHttpAdminCompanyDetail::$id);
    appHttpAdminCompanyDetail::$results = appFuncDatabase::getData(
        appFuncDatabase::connect(),
        appDatabaseCompany::$detail,
        appHttpAdminCompanyDetail::$params
    );
    appHttpAdminCompanyDetail::$name = appHttpAdminCompanyDetail::$results[0]['name'];
    appHttpAdminCompanyDetail::$address = appHttpAdminCompanyDetail::$results[0]['address'];
    appHttpAdminCompanyDetail::$tel = appHttpAdminCompanyDetail::$results[0]['tel'];
    appHttpAdminCompanyDetail::$area = appHttpAdminCompanyDetail::$results[0]['area'];
    appHttpAdminCompanyDetail::$comment = appHttpAdminCompanyDetail::$results[0]['comment'];
    appHttpAdminCompanyDetail::$insert_date = appHttpAdminCompanyDetail::$results[0]['insert_date'];
    appHttpAdminCompanyDetail::$update_date = appHttpAdminCompanyDetail::$results[0]['update_date'];
}


