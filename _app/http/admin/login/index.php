<?php
include_once '../../_app/ssl_base.php';
class appHttpAdminLoginIndex
{
    public static $postId = "";
    public static $postPassword = "";
    public static $sessionLoginStatus = "";
    public static $sessionLoginName = "";
    public static $h1text = "オペレーター";
    //エラーメッセージ
    public static $postResult = "";
    public static $errMsg = "";
}

if (isset($_POST['login'])) {
    appHttpAdminLoginIndex::$postId = $_POST['id'];
    appHttpAdminLoginIndex::$postPassword = $_POST['password'];
    appHttpAdminLoginIndex::$postResult = appLibraryLogin::checkLoginForm(appConfig::loginUser, $_POST['id'], $_POST['password']);
    var_dump(appHttpAdminLoginIndex::$postResult);
    switch (appHttpAdminLoginIndex::$postResult["errorCode"]) {
        case 0:
            appHttpAdminLoginIndex::$sessionLoginStatus = appConfig::loginSession[appConfig::$directory]['status'];
            appHttpAdminLoginIndex::$sessionLoginName = appConfig::loginSession[appConfig::$directory]['name'];
            $_SESSION[appHttpAdminLoginIndex::$sessionLoginStatus] = appConfig::$directory;
            $_SESSION[appHttpAdminLoginIndex::$sessionLoginName] = appHttpAdminLoginIndex::$postResult["userName"];
            header("Location:/" . appConfig::$directory);
            break;
        case 1:
            appHttpAdminLoginIndex::$errMsg .= '<div class="alert alert-danger" role="alert">【エラー】ログインID、またはパスワードが未入力です</div>';
            break;
        case 2:
            appHttpAdminLoginIndex::$errMsg .= '<div class="alert alert-danger" role="alert">【エラー】ログインID、またはパスワードが違います</div>';
            break;
    }
}
