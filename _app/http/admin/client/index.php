<?php
include_once '../../_app/ssl_base.php';
include_once '../../_app/database/client.php';

class appHttpAdminClientIndex
{
    public static $params = array(); //DBに送信するパラメータ
    public static $results = ""; //DBから戻ってきた「顧客情報」の値が格納される

    public static $id = "";
    public static $name = "";
    public static $update = "";

    function list($results)
    {
        $body = "";
        foreach ($results as $value) {
            $body .= '<tr>';
            $body .= '<td>' . $value['id'] . '</td>';
            $body .= '<td>' . $value['name'].'<br><span class="font-small">連絡先：' . $value['tel'] . '</span></td>';
            $body .= '<td class="text-center"><a href="./detail?id=' . $value['id'] . '">詳細</a></td>';
            $body .= '</tr>';
        }
        return $body;
    }
}

appHttpAdminClientIndex::$results = appFuncDatabase::getData(
    appFuncDatabase::connect(),
    appDatabaseClient::$list,
    appHttpAdminClientIndex::$params
);
