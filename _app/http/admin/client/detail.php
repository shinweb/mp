<?php
include_once '../../_app/ssl_base.php';
include_once '../../_app/database/client.php';
include_once '../../_app/database/order.php';

class appHttpAdminClientDetail
{
    public static $params = array(); //DBに送信するパラメータ
    public static $results = ""; //DBから戻ってきた「顧客情報」の値が格納される
    public static $params_order = array();
    public static $results_order = ""; //DBから戻ってきた「注文」の値が格納される

    public static $id = "";
    public static $name = "";
    public static $name_kana = "";
    public static $tel = "";
    public static $mail = "";
    public static $address = "";
    public static $comment = "";
    public static $insert_date = "";
    public static $update_date = "";

    
}

if (isset($_POST["id"]) && preg_match("/^[0-9]+$/", $_POST["id"])) {
    appHttpAdminClientDetail::$params = array(
        ':id' => $_POST["id"],
        ':name' => $_POST["name"],
        ':name_kana' =>  $_POST["name_kana"],
        ':address' =>  $_POST["address"],
        ':tel' =>  $_POST["tel"],
        ':mail' =>  $_POST["mail"],
        ':comment' =>  $_POST["comment"],
        ':update_date' => date("Y-m-d H:i:s"),
        ':update_by' =>  'ADMIN'
    );
    appFuncDatabase::updateData(appFuncDatabase::connect(), appDatabaseClient::$update, appHttpAdminClientDetail::$params);
}

if (isset($_GET["id"])) {
    appHttpAdminClientDetail::$params = array();
    appHttpAdminClientDetail::$id = preg_replace('/[^0-9]/', '',  $_GET["id"]);
    appHttpAdminClientDetail::$id = intval(appHttpAdminClientDetail::$id);
    appHttpAdminClientDetail::$params = array("id" => appHttpAdminClientDetail::$id);
    appHttpAdminClientDetail::$results = appFuncDatabase::getData(
        appFuncDatabase::connect(),
        appDatabaseClient::$detail,
        appHttpAdminClientDetail::$params
    );
    appHttpAdminClientDetail::$name = appHttpAdminClientDetail::$results[0]['name'];
    appHttpAdminClientDetail::$name_kana = appHttpAdminClientDetail::$results[0]['name_kana'];
    appHttpAdminClientDetail::$address = appHttpAdminClientDetail::$results[0]['address'];
    appHttpAdminClientDetail::$tel = appHttpAdminClientDetail::$results[0]['tel'];
    appHttpAdminClientDetail::$mail = appHttpAdminClientDetail::$results[0]['mail'];
    appHttpAdminClientDetail::$comment = appHttpAdminClientDetail::$results[0]['comment'];
    appHttpAdminClientDetail::$insert_date = appHttpAdminClientDetail::$results[0]['insert_date'];
    appHttpAdminClientDetail::$update_date = appHttpAdminClientDetail::$results[0]['update_date'];

    appDatabaseOrder::$list."";
    appHttpAdminClientDetail::$params_order = array("client_id" => appHttpAdminClientDetail::$results[0]["client_id"]);
    appHttpAdminClientDetail::$results_order = appFuncDatabase::getData(
        appFuncDatabase::connect(),
        appDatabaseOrder::$list,
        appHttpAdminClientDetail::$params_order
    );
}
