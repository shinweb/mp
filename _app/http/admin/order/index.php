<?php
include_once '../../_app/ssl_base.php';
include_once '../../_app/database/order.php';
include_once '../../_app/database/client.php';
appConfig::$uri = $_SERVER["REQUEST_URI"];

class appHttpAdminOrderIndex
{
    const pageLimit = 10;
    public static $params_order = array(); //DBに送信するパラメータ
    public static $params_client = array(); //DBに送信するパラメータ
    public static $results_order = ""; //DBから戻ってきた「注文情報」の値が格納される
    public static $results_client = array(); //DBから戻ってきた「顧客情報」の値が格納される

    public static $get_sort = 0;
    public static $get_words = '';
    public static $get_status = 'all';

    public static $sql_where = "";
    public static $sql_limit = "";
    public static $sql_select_order = "";
    public static $sql_select_client = "";

    public static $id = "";
    public static $name = "";
    public static $update = "";

    public static $pageCount = 0;
    public static $pageNum = 1;
    public static $pageOffset = 1;
    public static $msg = "";

    /*キーワード検索用のWHERE句を追加*/
    function searchWord($get_words)
    {
        //戻り値
        $result = "";
        //検索ワードを文字列置換（半角を全角に）
        $get_words = str_replace('　', ' ', $get_words);
        //検索ワードから空白文字を除去
        $get_words_trim = trim($get_words);
        //分岐…検索ワードの有無を判定
        if (mb_strlen($get_words_trim) > 0) {
            //検索ワードが有の場合…検索ワードを配列に変換
            $get_wordsExplode = explode(' ', $get_words);
            foreach ($get_wordsExplode as $value) {
                $result .= '(';
                $result .= <<<EOF
o.kojin_name like "%{$value}%" or
o.kojin_kana_name like "%{$value}%" or
o.kojin_address like "%{$value}%" or
o.comment like "%{$value}%" or
cl.name like "%{$value}%" or
cl.name_kana like "%{$value}%" or
cl.tel like "%{$value}%" or
cl.address like "%{$value}%" or
cl.mail like "%{$value}%" or
cp.name like "%{$value}%" or
cp.tel like "%{$value}%" or
cp.address like "%{$value}%" or
cp.comment like "%{$value}%"
EOF;
                if ($value === end($get_wordsExplode)) {
                    $result .= ')';
                } else {
                    $result .= ') and ';
                }
            }
        }
        return $result;
    }
    /*ステータス絞り込み用の関数*/
    function statusFilter($get)
    {
        $result = "";
        $array = appConfigPlan::plan;
        foreach ($array as $plan_value) {
            if ($get == $plan_value['id']) {
                $result .= ' status = ' . $plan_value['id'];
                break;
            }
            if ($plan_value === end($array)) {
                $result .= ' status is not null';
            }
        }
        return $result;
    }
    /*日時の処理*/
    function dateFormat($string)
    {
        $array = explode(',', $string);
        if (isset($array[0])) {
            $date = $array[0];
            $dateDisp = date('Y年m月d日', strtotime($date));
        }
        /*時*/
        $timeH = "--";
        if (isset($array[1])) {
            $timeH = $array[1];
        }
        /*分*/
        $timeI = "--";
        if (isset($array[2])) {
            $timeI = $array[2];
        }
        return $dateDisp . '<br>' . $timeH . '時' . $timeI . '分';
    }

    /*一覧表示*/
    function list($results_order, $results_client)
    {
        $body = "";
        $count = 0;
        foreach ($results_order as $value) {
            $day_sougi = appHttpAdminOrderIndex::dateFormat($value['day_sougi']);
            $day_sekou = appHttpAdminOrderIndex::dateFormat($value['day_sekou']);
            $update_date = date('Y年m月d日 H:i',  strtotime($value['update_date']));
            $insert_date = date('Y年m月d日 H:i',  strtotime($value['insert_date']));
            $update_by = $value['update_by'];
            if ($value['status'] == 4 || $value['status'] == 5) {
                $body .= '<tr class="bg-gray">';
            } else {
                $body .= '<tr>';
            }
            $body .= '<td class="text-center">' . $value['id'] . '</td>';
            foreach ($results_client as $value2) {
                if ($value['client_id'] == $value2['id']) {
                    $body .= '<td><a href="./detail?id=' . $value['id'] . '">' . $value2['name'] . '</a><br><span class="font-small">' . $value2['name_kana'] . '</span></td>';
                }
            }
            $body .= '<td class="font-small">' . appConfigPlan::status[$value['status']]['name']  . '</td>';
            $body .= '<td class="text-right">' . $day_sougi . '</td>';
            $body .= '<td class="text-right">' . $day_sekou . '</td>';
            $body .= '<td class="font-small">更新者：'.$update_by.'<br>更新日：' . $update_date . '<br>登録日：' . $insert_date .'</td>';
            $body .= '<td class="text-center"><a href="./detail?id=' . $value['id'] . '">詳細</a></td>';
            $body .= '</tr>';
            $count++;
        }
        return $body;
    }
}

/*
STEP1：SQL文作成1
WHERE句の作成
*/
//分岐：キーワード検索か通常検索か？
if (isset($_GET['words']) && mb_strlen($_GET['words']) > 0) {
    appHttpAdminOrderIndex::$get_words = htmlspecialchars($_GET["words"], ENT_QUOTES, "UTF-8");
    appHttpAdminOrderIndex::$sql_select_order = appDatabaseOrder::$searchWords;
    appHttpAdminOrderIndex::$sql_where .= appHttpAdminOrderIndex::searchWord(appHttpAdminOrderIndex::$get_words);
    appHttpAdminOrderIndex::$sql_where .= ' and ';
} else {
    appHttpAdminOrderIndex::$sql_select_order = appDatabaseOrder::$list;
}
//分岐：表示非表示のステータス
if (isset($_GET['status'])) {
    appHttpAdminOrderIndex::$get_status = $_GET['status'];
}
appHttpAdminOrderIndex::$sql_where .= appHttpAdminOrderIndex::statusFilter(appHttpAdminOrderIndex::$get_status);

/*
STEP2：SQL文作成2
ORDER BY句の作成
*/
if (isset($_GET['sort'])) {
    appHttpAdminOrderIndex::$get_sort = $_GET['sort'];
}
switch (appHttpAdminOrderIndex::$get_sort) {
    case 'update_date-desc':
        appHttpAdminOrderIndex::$sql_where .= ' order by update_date desc';
        break;
    case 'insert_date-desc':
        appHttpAdminOrderIndex::$sql_where .= ' order by insert_date desc';
        break;
    case 'update_date-asc':
        appHttpAdminOrderIndex::$sql_where .= ' order by update_date asc';
        break;
    case 'insert_date-asc':
        appHttpAdminOrderIndex::$sql_where .= ' order by insert_date asc';
        break;
    default:
        appHttpAdminOrderIndex::$sql_where .= ' order by id asc';
        break;
}

/*
STEP3：LIMIT句を作成
*/
if (isset($_GET['words']) && mb_strlen($_GET['words']) > 0) {
    appHttpAdminOrderIndex::$sql_limit .= ' limit 100';
    appHttpAdminOrderIndex::$msg = '検索キーワード　' . appHttpAdminOrderIndex::$get_words . '　を含む100件の注文データを表示します（100件以上は表示できません）';
} else {
    appHttpAdminOrderIndex::$pageCount = appFuncDatabase::getData(
        appFuncDatabase::connect(),
        appDatabaseOrder::$count . appHttpAdminOrderIndex::$sql_where,
        appHttpAdminOrderIndex::$params_order
    );
    appHttpAdminOrderIndex::$pageCount =  appLibraryArray::singleKeyExtraction(appHttpAdminOrderIndex::$pageCount, "count");
    if (isset($_GET["page"])) {
        appHttpAdminOrderIndex::$pageNum = intval($_GET["page"]);
    }
    appHttpAdminOrderIndex::$pageOffset = (appHttpAdminOrderIndex::$pageNum - 1) * appHttpAdminOrderIndex::pageLimit;
    appHttpAdminOrderIndex::$sql_limit .= ' limit ' . appHttpAdminOrderIndex::pageLimit . ' offset ' . appHttpAdminOrderIndex::$pageOffset;
}

/*
STEP4：注文情報を取得
*/
appHttpAdminOrderIndex::$sql_select_order .= appHttpAdminOrderIndex::$sql_where . appHttpAdminOrderIndex::$sql_limit;
appHttpAdminOrderIndex::$results_order = appFuncDatabase::getData(
    appFuncDatabase::connect(),
    appHttpAdminOrderIndex::$sql_select_order,
    appHttpAdminOrderIndex::$params_order
);

/*
STEP5:顧客情報を取得
STEP4で取得した注文情報の「client_id」をもとに取得する
*/
$count = 0;
appHttpAdminOrderIndex::$sql_select_client = appDatabaseClient::$list;
foreach (appHttpAdminOrderIndex::$results_order as $value) {
    if ($count == 0) {
        appHttpAdminOrderIndex::$sql_select_client .= ' WHERE ';
    }
    if ($value === end(appHttpAdminOrderIndex::$results_order)) {
        appHttpAdminOrderIndex::$sql_select_client .= ' id=' . $value['client_id'];
    } else {
        appHttpAdminOrderIndex::$sql_select_client .= ' id=' . $value['client_id'] . ' or ';
        $count++;
    }
}
appHttpAdminOrderIndex::$results_client = appFuncDatabase::getData(
    appFuncDatabase::connect(),
    appHttpAdminOrderIndex::$sql_select_client,
    appHttpAdminOrderIndex::$params_client
);
