<?php
include_once '../../_app/ssl_base.php';
include_once '../../_app/database/order.php';
include_once '../../_app/database/client.php';
include_once '../../_app/database/company.php';
include_once '../../_app/database/questionnaire.php';

class appHttpAdminOrderDetail
{
    //データの送受信に関する変数
    public static $posttype = "insert";
    public static $params = array(); //DBに送信するパラメータ（注文情報）
    public static $params_client = array(); //DBに送信するパラメータ（顧客情報）
    public static $params_questionnaire = array();
    public static $params_q1 = array(); //DBに送信するパラメータ（アンケート情報）
    public static $params_q2 = array(); //DBに送信するパラメータ（アンケート情報）
    public static $results; //DBから戻ってきた「顧客情報」の値が格納される
    public static $results_company;
    public static $results_questionnaire;
    public static $postFlag = false;
    //個人情報に関する変数
    public static $id;
    public static $repeater = 0;
    public static $name;
    public static $name_kana;
    public static $tel;
    public static $mail;
    public static $address = array('', '', '', '');
    //注文情報に関する変数
    public static $client_id;
    public static $company_id = 0;
    public static $plan_id = 0;
    public static $saijo_id = 0;
    public static $kasou_id = 0;
    public static $day_sougi = array('', '', '');
    public static $day_sekou = array('', '', '');
    public static $status = 0;
    public static $kojin_name;
    public static $kojin_kana_name;
    public static $kojin_address = array('', '', '', '');
    public static $int_ninzu = 0;
    public static $option_anti = 0;
    public static $option_obousan = 0;
    public static $option_bodaiji = 0;
    public static $option_frower_hitsugi = 0;
    public static $option_frower_kyouka = 0;
    public static $option_food_tuya = 0;
    public static $option_food_shoujin = 0;
    public static $option_henreihin = 0;
    public static $option_yukan = 0;
    public static $comment = "なし";
    public static $insert_date = "";
    public static $update_date = "";
    public static $update_by = "";
    public static $company_name = "";
    public static $company_address = "";
    public static $company_tel = "";
    //その他
    public static $msg = "";
    public static $get_postcomp = "";
    public static $errorCount = 0;
}
/*
分岐：SUBMITが実行されていた場合
*/
if (isset($_POST["posttype"])) {
    appHttpAdminOrderDetail::$params_client = array(
        ':name' => $_POST["name"],
        ':name_kana' =>  $_POST["name_kana"],
        ':tel' =>  $_POST["tel"],
        ':address' =>  $_POST["address1"] . ',' . $_POST["address2"] . ',' . $_POST["address3"] . ',' . $_POST["address4"],
        ':mail' =>  $_POST["mail"],
        ':update_date' => date("Y-m-d H:i:s"),
        ':update_by' => appConfig::$loginUserName
    );
    appHttpAdminOrderDetail::$params = array(
        ':repeater' =>  $_POST["repeater"],
        ':plan_id' =>  $_POST["plan_id"],
        ':company_id' =>  $_POST["company_id"],
        ':saijo_id' =>  $_POST["saijo_id"],
        ':kasou_id' =>  $_POST["kasou_id"],
        ':day_sougi' =>  $_POST["day_sougi1"] . ',' . $_POST["day_sougi2"] . ',' . $_POST["day_sougi3"],
        ':day_sekou' =>  $_POST["day_sekou1"] . ',' . $_POST["day_sekou2"] . ',' . $_POST["day_sekou3"],
        ':status' =>  $_POST["status"],
        ':kojin_name' =>  $_POST["kojin_name"],
        ':kojin_kana_name' =>  $_POST["kojin_kana_name"],
        ':kojin_address' => $_POST["kojin_address1"] . ',' . $_POST["kojin_address2"] . ',' . $_POST["kojin_address3"] . ',' . $_POST["kojin_address4"],
        ':int_ninzu' =>  $_POST["int_ninzu"],
        ':option_anti' =>  $_POST["option_anti"],
        ':option_obousan' =>  $_POST["option_obousan"],
        ':option_bodaiji' =>  $_POST["option_bodaiji"],
        ':option_frower_hitsugi' =>  $_POST["option_frower_hitsugi"],
        ':option_frower_kyouka' =>  $_POST["option_frower_kyouka"],
        ':option_food_tuya' =>  $_POST["option_food_tuya"],
        ':option_food_shoujin' =>  $_POST["option_food_shoujin"],
        ':option_henreihin' =>  $_POST["option_henreihin"],
        ':option_yukan' =>  $_POST["option_yukan"],
        ':comment' =>  $_POST["comment"],
        ':update_date' => date("Y-m-d H:i:s"),
        ':update_by' =>  appConfig::$loginUserName
    );
    /*
    SUBMITが実行されていた場合＝＞顧客データの処理
     */
    if (isset($_POST["repeater"]) && $_POST["repeater"] == 0) {
        /*新規顧客*/
        /*テーブル「client」：データ新規追加*/
        $results_latest_client = appFuncDatabase::getData(
            appFuncDatabase::connect(),
            appDatabaseClient::$latest,
            appHttpAdminOrderDetail::$params_client
        );
        appHttpAdminOrderDetail::$params_client[":id"] = intval($results_latest_client[0]['id']) + 1;
        appHttpAdminOrderDetail::$params_client[":insert_date"] = date("Y-m-d H:i:s");
        appFuncDatabase::insertData(appFuncDatabase::connect(), appDatabaseClient::$insert, appHttpAdminOrderDetail::$params_client);
    } else if (isset($_POST["repeater"]) && $_POST["repeater"] == 1) {
        /*既存顧客*/
        /*テーブル「client」：データ更新*/
        appHttpAdminOrderDetail::$params_client[":id"] = $_POST["client_id"];
        appFuncDatabase::updateData(appFuncDatabase::connect(), appDatabaseClient::$update, appHttpAdminOrderDetail::$params_client);
    } else {
        /*新規でも既存顧客でもない（ありえない状況）*/
        appHttpAdminOrderDetail::$msg = '<div class="alert alert-danger" role="alert">エラー：入力された顧客情報が不正です</div>';
        $errorCount++;
    }
    /*
    SUBMITが実行されていた場合＝＞注文データ・アンケートデータの処理
     */
    appHttpAdminOrderDetail::$client_id = appHttpAdminOrderDetail::$params_client[":id"];
    if ($_POST["posttype"] == "update") {
        /*アップデートの場合*/
        /*注文情報*/
        appHttpAdminOrderDetail::$params[":id"] = $_POST["id"];
        appHttpAdminOrderDetail::$params[":client_id"] = appHttpAdminOrderDetail::$client_id;
        appFuncDatabase::updateData(appFuncDatabase::connect(), appDatabaseOrder::$update, appHttpAdminOrderDetail::$params);
    } else if ($_POST["posttype"] == "insert") {
        /*新規注文の場合*/
        /*テーブル「order」：データ新規追加*/
        $results_latest_order = appFuncDatabase::getData(
            appFuncDatabase::connect(),
            appDatabaseOrder::$latest,
            appHttpAdminOrderDetail::$params
        );
        appHttpAdminOrderDetail::$params[":id"] = intval($results_latest_order[0]['id']) + 1;
        appHttpAdminOrderDetail::$params[":client_id"] = appHttpAdminOrderDetail::$client_id;
        appHttpAdminOrderDetail::$params[":insert_date"] = date("Y-m-d H:i:s");
        appFuncDatabase::insertData(appFuncDatabase::connect(), appDatabaseOrder::$insert, appHttpAdminOrderDetail::$params);
    }
    if (appHttpAdminOrderDetail::$errorCount == 0) {
        header('Location: /admin/order/detail?id=' . appHttpAdminOrderDetail::$params[":id"] . '&postcomp=success');
    }
}

/*
分岐：「登録完了」のパラメータがURLにある場合
*/
if (isset($_GET["postcomp"])) {
    appHttpAdminOrderDetail::$get_postcomp = $_GET["postcomp"];
    switch (appHttpAdminOrderDetail::$get_postcomp) {
        case 'success':
            appHttpAdminOrderDetail::$msg = '<div class="alert alert-success" role="alert">更新しました</div>';
            break;
    }
}

/*
処理：データの受信
*/
if (isset($_GET["id"])) {
    /*
    取引情報取得
    */
    appHttpAdminOrderDetail::$posttype = "update";
    appHttpAdminOrderDetail::$params = array();
    appHttpAdminOrderDetail::$id = preg_replace('/[^0-9]/', '',  $_GET["id"]);
    appHttpAdminOrderDetail::$id = intval(appHttpAdminOrderDetail::$id);
    appHttpAdminOrderDetail::$params = array("id" => appHttpAdminOrderDetail::$id);
    $results_order = appFuncDatabase::getData(
        appFuncDatabase::connect(),
        appDatabaseOrder::$detail,
        appHttpAdminOrderDetail::$params
    );
    /*
    顧客情報取得
    */
    appHttpAdminOrderDetail::$params = array("id" => $results_order[0]['client_id']);
    $results_client = appFuncDatabase::getData(
        appFuncDatabase::connect(),
        appDatabaseClient::$detail,
        appHttpAdminOrderDetail::$params
    );
    appHttpAdminOrderDetail::$name = $results_client[0]['name'];
    appHttpAdminOrderDetail::$name_kana = $results_client[0]['name_kana'];
    appHttpAdminOrderDetail::$tel = $results_client[0]['tel'];
    appHttpAdminOrderDetail::$address = explode(',', $results_client[0]['address']);
    appHttpAdminOrderDetail::$mail = $results_client[0]['mail'];
    appHttpAdminOrderDetail::$repeater = $results_order[0]['repeater'];
    appHttpAdminOrderDetail::$client_id = $results_order[0]['client_id'];
    appHttpAdminOrderDetail::$company_id = $results_order[0]['company_id'];
    appHttpAdminOrderDetail::$plan_id = $results_order[0]['plan_id'];
    appHttpAdminOrderDetail::$saijo_id = $results_order[0]['saijo_id'];
    appHttpAdminOrderDetail::$kasou_id = $results_order[0]['kasou_id'];
    appHttpAdminOrderDetail::$day_sougi = explode(',', $results_order[0]['day_sougi']);
    appHttpAdminOrderDetail::$day_sekou = explode(',', $results_order[0]['day_sekou']);
    appHttpAdminOrderDetail::$status = $results_order[0]['status'];
    appHttpAdminOrderDetail::$kojin_name = $results_order[0]['kojin_name'];
    appHttpAdminOrderDetail::$kojin_kana_name = $results_order[0]['kojin_kana_name'];
    appHttpAdminOrderDetail::$kojin_address = explode(',', $results_order[0]['kojin_address']);
    appHttpAdminOrderDetail::$int_ninzu = $results_order[0]['int_ninzu'];
    appHttpAdminOrderDetail::$option_anti = $results_order[0]['option_anti'];
    appHttpAdminOrderDetail::$option_obousan = $results_order[0]['option_obousan'];
    appHttpAdminOrderDetail::$option_bodaiji = $results_order[0]['option_bodaiji'];
    appHttpAdminOrderDetail::$option_frower_hitsugi = $results_order[0]['option_frower_hitsugi'];
    appHttpAdminOrderDetail::$option_frower_kyouka = $results_order[0]['option_frower_kyouka'];
    appHttpAdminOrderDetail::$option_food_tuya = $results_order[0]['option_food_tuya'];
    appHttpAdminOrderDetail::$option_food_shoujin = $results_order[0]['option_food_shoujin'];
    appHttpAdminOrderDetail::$option_henreihin = $results_order[0]['option_henreihin'];
    appHttpAdminOrderDetail::$option_yukan = $results_order[0]['option_yukan'];
    appHttpAdminOrderDetail::$comment = $results_order[0]['comment'];
    appHttpAdminOrderDetail::$insert_date = $results_order[0]['insert_date'];
    appHttpAdminOrderDetail::$update_date = $results_order[0]['update_date'];
    appHttpAdminOrderDetail::$update_by = $results_order[0]['update_by'];
    /*
    葬儀社情報取得
    */
    if (appHttpAdminOrderDetail::$company_id != 0) {
        appHttpAdminOrderDetail::$params = array("id" => appHttpAdminOrderDetail::$company_id);
        $results_company = appFuncDatabase::getData(
            appFuncDatabase::connect(),
            appDatabaseCompany::$detail,
            appHttpAdminOrderDetail::$params
        );
        appHttpAdminOrderDetail::$company_name = $results_company[0]['name'];
        appHttpAdminOrderDetail::$company_address = $results_company[0]['address'];
        appHttpAdminOrderDetail::$company_tel = $results_company[0]['tel'];
    }
}