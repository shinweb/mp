<?php
include_once '../../_app/ssl_base.php';
class appHttpMypageLoginIndex
{
    public static $postId = "";
    public static $postPassword = "";
    public static $sessionLoginStatus = "";
    public static $sessionLoginName = "";
    public static $h1text = "企業様";
    //エラーメッセージ
    public static $postResult = "";
    public static $errMsg = "";
}

if (isset($_POST['login'])) {
    appHttpMypageLoginIndex::$postId = $_POST['id'];
    appHttpMypageLoginIndex::$postPassword = $_POST['password'];
    appHttpMypageLoginIndex::$postResult = appLibraryLogin::checkLoginForm(appConfig::loginCompany, $_POST['id'], $_POST['password']);
    switch (appHttpMypageLoginIndex::$postResult["errorCode"]) {
        case 0:
            appHttpMypageLoginIndex::$sessionLoginStatus = appConfig::loginSession[appConfig::$directory]['status'];
            appHttpMypageLoginIndex::$sessionLoginName = appConfig::loginSession[appConfig::$directory]['name'];
            $_SESSION[appHttpMypageLoginIndex::$sessionLoginStatus] = appConfig::$directory;
            $_SESSION[appHttpMypageLoginIndex::$sessionLoginName] = appHttpMypageLoginIndex::$postResult["userName"];
            header("Location:/" . appConfig::$directory);
            break;
        case 1:
            appHttpMypageLoginIndex::$errMsg = '<div class="alert alert-danger" role="alert">【エラー】ログインID、またはパスワードが未入力です</div>';
            break;
        case 2:
            appHttpMypageLoginIndex::$errMsg = '<div class="alert alert-danger" role="alert">【エラー】ログインID、またはパスワードが違います</div>';
            break;
    }
}

if (isset($_POST['login'])) {
}
