<?php
//汎用的な数値や変数を呼び出す
include_once __DIR__ . '/define.php'; //開発環境、本番環境で値が変わる値（定数）をここで定義
include_once __DIR__ . '/config/base.php'; //汎用的なテキストや数値を呼ぶ
include_once __DIR__ . '/config/plan.php'; //プランに関する情報を呼ぶ
include_once __DIR__ . '/config/form.php'; //プランに関する情報を呼ぶ
include_once __DIR__ . '/library/database.php'; //DBにデータの送受信を行うときに必要
include_once __DIR__ . '/library/array.php'; //DBにデータの送受信を行うときに必要
include_once __DIR__ . '/library/form.php'; //DBにデータの送受信を行うときに必要
include_once __DIR__ . '/library/nav.php'; //DBにデータの送受信を行うときに必要