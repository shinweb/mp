<!doctype html>
<html lang="ja">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?php if (appConfig::$tmpl == "top") : ?>
        <title><?php echo appConfig::$title; ?></title>
    <?php else : ?>
        <title><?php echo appConfig::$title; ?>｜<?php echo appConfig::companyName; ?></title>
    <?php endif; ?>

    <meta name="description" content="<?php echo appConfig::$description; ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="format-detection" content="telephone=no">
    <meta property="og:type" content="website">
    <meta property="og:title" content="<?php echo appConfig::$title; ?>>">
    <meta property="og:url" content="<?php echo appConfig::baseUrl; ?>">
    <meta property="og:site_name" content="<?php echo appConfig::companyName; ?>">
    <meta property="og:description" content="<?php echo appConfig::$description; ?>">
    <meta property="og:locale" content="ja_JP">
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet" onload="this.media='all'" />
    <link href="/assets/css/font-awesome.min.css" rel="stylesheet" onload="this.media='all'" />
    <link href="/assets/css/common.css?20211118" rel="stylesheet" onload="this.media='all'" />
    <?php echo appConfig::$css; ?>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script>
        window.jQuery || document.write('<script src="/asset/js/jquery-3.3.1.min.js"><\/script>');
    </script>

    <?php echo appConfig::$js; ?>

</head>

<body id="top">