</main>
<script src="/assets/js/bootstrap.bundle.min.js"></script>
<?php if (appConfig::$tmpl != "detail_right") : ?>
    <footer class="border-top p-3 text-center bg-dark text-white">株式会社Lab.u</footer>
<?php endif; ?>
<script>
    $(function() {
        /*セレクトメニュー切り替え：都道府県*/
        $('[data-select-area]').change(function(e) {
            dataSelectArea();
        });
        /*関数：都道府県切り替え*/
        function dataSelectArea() {
            let area = $('[data-select-area]').val();
            $.ajax({
                    type: "GET",
                    url: "/admin/ajax/select_city?area=" + area,
                    dataType: "html"
                })
                // Ajaxリクエストが成功した場合
                .done(function(data) {
                    $('#select-city').empty().html(data);
                    return true;
                })
                // Ajaxリクエストが失敗した場合
                .fail(function(XMLHttpRequest, textStatus, errorThrown) {
                    alert('エラーが発生しました');
                    return false;
                });
        }
        $("input").keydown(function(e) {
            if ((e.which && e.which === 13) || (e.keyCode && e.keyCode === 13)) {
                return false;
            } else {
                return true;
            }
        });
    });
</script>
</body>

</html>