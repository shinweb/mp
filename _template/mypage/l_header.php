<?php if (appConfig::$tmpl != "detail_right") : ?>
    <header>
        <nav class="navbar fixed-top navbar-light bg-light border-bottom">
            <div class="navbar-brand" href="#">
                <?php echo appConfig::siteCategory[appConfig::$directory]['title']; ?>
            </div>
            <?php if (appConfig::$tmpl != "login") : ?>
                <form method="post" class="form-inline">
                    <input type="hidden" name="logout">
                    <span class="pr-3"><?php echo appConfig::$loginUserName; ?></span>
                    <button class="btn btn-outline-secondary"><i class="fa fa-sign-out mr-1" aria-hidden="true"></i>ログアウト</button>
                </form>
            <?php endif; ?>
        </nav>
    </header>
<?php endif; ?>

<?php if (appConfig::$tmpl == "detail") : ?>
    <main class="bg-lgray" style="padding-top:50px; min-height:1000px;">
    <?php elseif (appConfig::$tmpl == "detail_right") : ?>
        <main class="bg-lgray" style="padding-top:60px; min-height:1000px;">
        <?php else : ?>
            <main style="padding-top:120px; min-height:1000px;">
            <?php endif; ?>