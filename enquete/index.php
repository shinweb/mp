<?php
include_once '../_app/http/enquete/index.php';
appConfig::$title = 'アンケート';
include_once '../_template/head.php';
include_once '../_template/l_header.php';
?>

<h1 class="text-center pb-2">アンケートページ</h1>
<p class="text-center pb-3">当社サービスをご利用の方にアンケートのご協力をお願いしています</p>
<?php echo appHttpEnquete::$msg; ?>
<div class="container">
    <div class="p-3 bg-white border">
        <?php if (appHttpEnquete::$disp === appHttpEnquete::$dispStatusNoDisp) : ?>
            データがありません
        <?php elseif (appHttpEnquete::$disp === appHttpEnquete::$dispStatusUnSent) : ?>
            <form method="post">
                <h2 class="pt-2 pb-2 font-xxmiddle text-center">サービス</h2>
                <div class="pb-5 text-center"><?php echo appLibraryForm::radio('service', '', appConfigform::point); ?></div>
                <h2 class="pb-2 font-xxmiddle text-center">価格</h2>
                <div class="pb-5 text-center"><?php echo appLibraryForm::radio('price', '', appConfigform::point); ?></div>
                <h2 class="pb-2 font-xxmiddle text-center">コメント</h2>
                <div class="pb-5 text-center">
                    <textarea class="form-control" name="comment" rows="3"></textarea>
                </div>
                <div class="p-2">
                    <input type="hidden" name="order_id" value="<?php echo appHttpEnquete::$order_id; ?>">
                    <button id="btn-update" type="submit" class="btn btn-lg btn-primary mx-auto d-block" data-toggle>アンケートを送信</button>
                </div>
            </form>
        <?php elseif (appHttpEnquete::$disp === appHttpEnquete::$dispStatusThanks) : ?>
            アンケートを受け付けました
        <?php elseif (appHttpEnquete::$disp === appHttpEnquete::$dispStatusSent) : ?>
            アンケートは送信済みです
        <?php elseif (appHttpEnquete::$disp === appHttpEnquete::$dispStatusTimeLimit) : ?>
            データがありません
        <?php endif; ?>
    </div>
    <p class="d-none"><?php echo appHttpEnquete::$flg; ?></p>
</div>

<?php
include_once '../_template/l_footer.php';
?>