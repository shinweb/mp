<?php
include_once '../../_app/http/admin/company/index.php';
appConfig::$tmpl = 'detail_right';
appConfig::$title = '葬儀会社一覧';
include_once '../../_template/admin/head.php';
include_once '../../_template/admin/l_header.php';
?>

<article>
    <div class="container">
        <h1 class="font-xxmiddle pb-3"><?php echo appConfig::$title; ?></h1>

        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">対応地域検索</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">フリーワード検索</a>
            </li>
        </ul>
        <div class="tab-content pb-3" id="myTabContent">
            <div class="tab-pane fade show active bg-white pt-2 pb-2 pl-4 pr-4" id="home" role="tabpanel" aria-labelledby="home-tab">
                <form method="get">
                    <div class="pb-4 pt-4">
                        <div class="pb-3">
                            <?php echo appLibraryForm::selectmenu_area('area', appConfig::area, appHttpAdminCompanyIndex::$get_area); ?>
                        </div>
                        <div class="pb-3" id="select-city">
                            <?php if (appHttpAdminCompanyIndex::$selectmenu === null) : ?>
                                都道府県を選択してください
                                <input type="hidden" name="city" value="">
                            <?php else : ?>
                                <?php echo appHttpAdminCompanyIndex::$selectmenu; ?>
                            <?php endif; ?>
                        </div>
                        <div class="pb-2">
                            <input type="hidden" name="posttype" value="area">
                            <button class="btn btn-secondary w-100"><i class="fa fa-search mr-2" aria-hidden="true"></i>検索</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">作成中です</div>
        </div>
        <table class="table table-bordered bg-white">
            <tr>
                <th class="bg-lgray w-50px">ランク</th>
                <th class="bg-lgray">情報</th>
            </tr>
            <?php echo appHttpAdminCompanyIndex::$table; ?>
        </table>
    </div>
</article>

<?php
include_once '../../_template/l_footer.php';
?>