<?php
include_once '../../_app/http/admin/company/detail.php';
appConfig::$tmpl = 'detail_right';
appConfig::$title = '会社情報詳細';
include_once '../../_template/admin/head.php';
include_once '../../_template/admin/l_header.php';
?>

<article>
    <div class="container">
        <p><a href="#" onclick="window.history.back(); return false;">BACK</a></p>
        <h1 class="font-xxmiddle pb-1"><?php echo appConfig::$title; ?></h1>
        <p class="font-small pb-1">登録日：<?php echo appHttpAdminCompanyDetail::$insert_date; ?>／更新日：<?php echo appHttpAdminCompanyDetail::$update_date; ?></p>
        <div class="bg-white p-3">
            <form method="POST">
                <?php echo appLibraryForm::text('葬儀社ID', 'id', appHttpAdminCompanyDetail::$id); ?>
                <?php echo appLibraryForm::text('名前', 'name', appHttpAdminCompanyDetail::$name); ?>
                <?php echo appLibraryForm::text('電話番号', 'tel', appHttpAdminCompanyDetail::$tel); ?>
                <?php echo appLibraryForm::text('住所', 'address', appHttpAdminCompanyDetail::$address); ?>
                <?php echo appLibraryForm::text('対応地区', 'area', appHttpAdminCompanyDetail::$area); ?>
                <?php echo appLibraryForm::text('コメント', 'comment', appHttpAdminCompanyDetail::$comment); ?>
            </form>
        </div>
    </div>
</article>

<?php
include_once '../../_template/l_footer.php';
?>