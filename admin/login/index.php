<?php
include_once '../../_app/http/admin/login/index.php';
appConfig::$tmpl = 'login';
appConfig::$title = 'ログインページ';
appConfig::$css = <<<EOF
<style>body{background-color:#eee;}</style>
EOF;
include_once '../../_template/admin/head.php';
include_once '../../_template/admin/l_header.php';
?>

<?php
$title  = 'ログイン（LAB.U オペレーター）';
$errMsg = appHttpAdminLoginIndex::$errMsg;
$appendHtml = "";
include_once '../../_module/form_login.php';
?>

<div class="container pt-5">
    <?php
    foreach (appConfig::loginUser as $value) {
        echo 'ID：' . $value["mail"] . '／PASS：' . $value["pass"] . '<br>';
    }
    ?>
</div>

<?php
include_once '../../_template/l_footer.php';
?>