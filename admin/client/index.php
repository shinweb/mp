<?php
include_once '../../_app/http/admin/client/index.php';
appConfig::$tmpl = 'detail_right';
appConfig::$title = '顧客情報一覧';
include_once '../../_template/admin/head.php';
include_once '../../_template/admin/l_header.php';
?>

<article>
    <div class="container">
        <h1 class="font-xxmiddle pb-3"><?php echo appConfig::$title; ?></h1>
        <table class="table table-bordered bg-white">
            <tr class="bg-lgray">
                <th class="text-center">ID</th>
                <th class="text-center">氏名</th>
                <th class="text-center">詳細</th>
            </tr>
            <?php echo appHttpAdminClientIndex::list(appHttpAdminClientIndex::$results); ?>
        </table>
    </div>
</article>

<?php
include_once '../../_template/l_footer.php';
?>