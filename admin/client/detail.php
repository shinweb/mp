<?php
include_once '../../_app/http/admin/client/detail.php';
appConfig::$tmpl = 'detail_right';
appConfig::$title = '顧客情報詳細';
include_once '../../_template/admin/head.php';
include_once '../../_template/admin/l_header.php';
?>

<article>
    <div class="container">
        <p><a href="./index">BACK</a></p>
        <h1><?php echo appConfig::$title; ?></h1>
        <p>登録日：<?php echo appHttpAdminClientDetail::$insert_date; ?>／更新日：<?php echo appHttpAdminClientDetail::$update_date; ?></p>
        <div class="bg-white p-3">
            <form method="POST">
                <?php echo appLibraryForm::text('顧客ID', 'id', appHttpAdminClientDetail::$id); ?>
                <?php echo appLibraryForm::text('名前', 'name', appHttpAdminClientDetail::$name); ?>
                <?php echo appLibraryForm::text('名前（カナ）', 'name_kana', appHttpAdminClientDetail::$name_kana); ?>
                <?php echo appLibraryForm::text('電話番号', 'tel', appHttpAdminClientDetail::$tel); ?>
                <?php echo appLibraryForm::text('住所', 'address', appHttpAdminClientDetail::$address); ?>
                <?php echo appLibraryForm::text('メール', 'mail', appHttpAdminClientDetail::$mail); ?>
            </form>
        </div>
    </div>
</article>

<?php
include_once '../../_template/l_footer.php';
?>