<?php
include_once '../_app/ssl_base.php';
appConfig::$title = '管理画面トップ';
include_once '../_template/admin/head.php';
include_once '../_template/admin/l_header.php';
?>
<div class="container">
    <h1 class="pb-4">管理画面</h1>
    <div class="row pb-3">
        <div class="col-12 text-center"><a href="/admin/order" class="p-4 border d-block">注文管理</a></div>
    </div>
    <div class="row pb-3">
        <div class="col-4 text-center"><a href="/admin/client" class="p-4 border d-block">顧客管理</a></div>
        <div class="col-4 text-center"><a href="/admin/company" class="p-4 border d-block">葬儀社管理</a></div>
        <div class="col-4 text-center"><a href="#" class="p-4 border d-block">斎場管理（未作成）</a></div>
    </div>
    <div class="row pb-3">
        <div class="col-4 text-center"><a href="#" class="p-4 border d-block">火葬場管理（未作成）</a></div>
        <div class="col-4 text-center"><a href="#" class="p-4 border d-block">アンケート管理（未作成）</a></div>
        <div class="col-4 text-center"></div>
    </div>
</div>

<?php
include_once '../_template/l_footer.php';
?>