<?php
include_once '../../_app/http/admin/news/edit.php';
appConfig::$tmp = 'admin';
appConfig::$title = 'お知らせの管理・更新';
appConfig::$css = <<<EOF
<link href="/asset/css/quill.snow.css" rel="stylesheet" />
<link href="/asset/css/admin.css" rel="stylesheet" />
EOF;
include_once '../../_template/admin/head.php';
include_once '../../_template/admin/l_header_admin.php';
?>

<?php appViewsHeading::h1Text(appConfig::$title); ?>
<?php appViewsAdmin::alertSuccess(appHttpAdminNewsEdit::$submit); ?>

<div class="container pdt-middle pdb-xlarge">
    <form id="form" method="post">
        <div class="pdb-large">
            <?php appViewsHeading::h3Text('タイトル<span class="badge badge-secondary ml-3">必須</span>'); ?>
            <input type="text" class="form-control" name="title" value="<?php echo appHttpAdminNewsEdit::$title; ?>">
        </div>
        <div class="pdb-large">
            <?php appViewsHeading::h3Text('更新日時<span class="badge badge-secondary ml-3">必須</span>'); ?>
            <input type="datetime-local" class="form-control w-50per" name="date" value="<?php echo appHttpAdminNewsEdit::$date; ?>">
        </div>
        <div class="pdb-large">
            <?php appViewsHeading::h3Text('カテゴリ<span class="badge badge-secondary ml-3">必須</span>'); ?>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="category" id="category0" value="0" <?php echo appFuncTextformat::searchReplaceEcho('0', 'checked', appHttpAdminNewsEdit::$category); ?>>
                <label class="form-check-label" for="category0">
                    お知らせ
                </label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="category" id="category1" value="1" <?php echo appFuncTextformat::searchReplaceEcho('1', 'checked', appHttpAdminNewsEdit::$category); ?>>
                <label class="form-check-label" for="category1">
                    重要<span class="color-highlight pl-2">（※個人情報流出、臨時休業、システムトラブル、当社の名前を語る詐欺グループ出現など異常事態のとき使用）</span>
                </label>
            </div>
        </div>
        <div class="pdb-large">
            <?php appViewsHeading::h3Text('表示／非表示<span class="badge badge-secondary ml-3">必須</span>'); ?>
            <p class="pdb-small">「非表示」を設定すると記事がお客様から見えなくなります</p>
            <div class="w-25">
                <select name="disp" class="form-control">
                    <option value="1" <?php echo appFuncTextformat::searchReplaceEcho('1', 'selected ', appHttpAdminNewsEdit::$disp); ?>>表示</option>
                    <option value="0" <?php echo appFuncTextformat::searchReplaceEcho('0', 'selected ', appHttpAdminNewsEdit::$disp); ?>>非表示</option>
                </select>
            </div>
        </div>
        <div class="pdb-large">
            <?php appViewsHeading::h3Text('遷移先URL'); ?>
            <input type="text" class="form-control" name="link" value="<?php echo appHttpAdminNewsEdit::$link; ?>" placeholder="例）/about/3melit">
        </div>
        <div class="pd-xlarge">
            <input type="hidden" name="id" value="<?php echo appHttpAdminNewsEdit::$id; ?>">
            <input type="hidden" name="posttype" value="<?php echo appHttpAdminNewsEdit::$postType; ?>">
            <button type="submit" class="btn btn-primary btn-lg w-300px mx-auto d-block" data-post>登録</button>
        </div>
    </form>

    <div class="modal fade" id="modal_insertimg" tabindex="-1" role="dialog" aria-labelledby="modal_insertimgLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal_insertimgLabel">追加する画像を選択してください</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <?php
                    //ファイル名表示処理
                    //ページング処理
                    //スクロール処理
                    //画像追加ボタンどうやって移動させる？
                    $dir = '../../asset/upload/';
                    $filelist = glob($dir . '*');
                    foreach ($filelist as $file) {
                        if (is_file($file)) {
                            $filepath = '/' . str_replace('../', '', $file);
                            echo '<div class="w-200px d-inline-block bg-lgray m-2"><img src="' . $filepath . '" class="w-100per h-auto" onClick=insertImage("' . $file . '") /><br>' . $file . '</div>';
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>




</div>


<?php
include_once '../../_template/l_footer_admin.php';
?>