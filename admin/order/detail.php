<?php
include_once '../../_app/http/admin/order/detail.php';
appConfig::$tmpl = 'detail';
appConfig::$title = '注文情報詳細';
include_once '../../_template/admin/head.php';
include_once '../../_template/admin/l_header.php';
?>

<article>
    <div class="container-fluid p-0">
        <div class="row no-gutters">
            <div class="col-8 bg-white border-right">
                <section id="sec_input">
                    <header class="p-3 pb-4 border-bottom">
                        <div class="w-lg-800px mx-auto">
                            <a href="./">BACK</a>
                            <h1><?php echo appConfig::$title; ?></h1>
                            <?php if (appHttpAdminOrderDetail::$posttype == "update") : ?>
                                <p>登録日：<?php echo appHttpAdminOrderDetail::$insert_date; ?>&nbsp;／&nbsp;更新日：<?php echo appHttpAdminOrderDetail::$update_date; ?>&nbsp;（更新者：<?php echo appHttpAdminOrderDetail::$update_by; ?>）</p>
                            <?php endif; ?>
                            <button id="btn-update" type="button" class="btn btn-lg btn-primary mx-auto" data-toggle>登録内容を変更</button>
                        </div>
                    </header>
                    <form method="POST" id="formarea" class="w-lg-800px mx-auto pt-5">
                        <?php echo appHttpAdminOrderDetail::$msg; ?>
                        <div class="pb-5">
                            <?php if (appHttpAdminOrderDetail::$posttype == "update") : ?>
                                <?php echo appLibraryForm::selectmenu('ステータス', 'status', appHttpAdminOrderDetail::$status, appConfigPlan::status); ?>
                            <?php endif; ?>
                        </div>
                        <section class="pb-5">
                            <h2 class="font-xxmiddle pb-3">葬儀の内容</h2>
                            <?php echo appLibraryForm::date('葬儀希望日', 'day_sougi', appHttpAdminOrderDetail::$day_sougi); ?>
                            <?php echo appLibraryForm::date('施行日', 'day_sekou', appHttpAdminOrderDetail::$day_sekou); ?>
                            <div class="row pt-3 pb-3 border-bottom">
                                <div class="col-4">ご遺体の安置場所</div>
                                <div class="col-8">
                                    <?php echo appLibraryForm::text('郵便番号<span class="font-small color-gray">（数字のみ）</span>', 'kojin_address1', appHttpAdminOrderDetail::$kojin_address[0]); ?>
                                    <?php echo appLibraryForm::text('都道府県', 'kojin_address2', appHttpAdminOrderDetail::$kojin_address[1]); ?>
                                    <?php echo appLibraryForm::text('市区町村', 'kojin_address3', appHttpAdminOrderDetail::$kojin_address[2]); ?>
                                    <?php echo appLibraryForm::text('番地', 'kojin_address4', appHttpAdminOrderDetail::$kojin_address[3]); ?>
                                </div>
                            </div>
                            <?php echo appLibraryForm::selectmenu('ご希望の葬儀プラン', 'plan_id', appHttpAdminOrderDetail::$plan_id, appConfigPlan::plan); ?>
                            <h3 class="pt-4 pb-2 font-xmiddle font-weight-bold">葬儀オプション</h3>
                            <?php echo appLibraryForm::selectmenu('安置', 'option_anti', appHttpAdminOrderDetail::$option_anti, appConfigPlan::anti); ?>
                            <?php echo appLibraryForm::selectmenu('菩提寺の有無', 'option_bodaiji', appHttpAdminOrderDetail::$option_bodaiji, appConfigPlan::bodaiji); ?>
                            <?php echo appLibraryForm::selectmenu('お坊さんの手配', 'option_obousan', appHttpAdminOrderDetail::$option_obousan, appConfigPlan::obousan); ?>
                            <?php echo appLibraryForm::text('棺内花の数', 'option_frower_hitsugi', appHttpAdminOrderDetail::$option_frower_hitsugi, '盆'); ?>
                            <?php echo appLibraryForm::text('ご供花の数', 'option_frower_kyouka', appHttpAdminOrderDetail::$option_frower_kyouka,  '本'); ?>
                            <?php echo appLibraryForm::text('参列者数', 'int_ninzu', appHttpAdminOrderDetail::$int_ninzu, '人'); ?>
                            <div class="row pt-3 pb-3 border-bottom">
                                <div class="col-4">食事・返礼品</div>
                                <div class="col-8">
                                    <?php echo appLibraryForm::text('通夜振る舞い', 'option_food_tuya', appHttpAdminOrderDetail::$option_food_tuya,  '人分'); ?>
                                    <?php echo appLibraryForm::text('精進落とし', 'option_food_shoujin', appHttpAdminOrderDetail::$option_food_shoujin,  '人分'); ?>
                                    <?php echo appLibraryForm::text('返礼品の数', 'option_henreihin', appHttpAdminOrderDetail::$option_henreihin,  '人分'); ?>
                                </div>
                            </div>
                            <?php echo appLibraryForm::selectmenu('湯かん', 'option_yukan', appHttpAdminOrderDetail::$option_yukan, appConfigPlan::yukan); ?>
                        </section>
                        <section class="pb-5">
                            <h2 class="font-xxmiddle pb-3">個人情報</h2>
                            <?php

                            $parm = array('required' => true);
                            echo appLibraryForm::radio_editPage('新規・リピーター', 'repeater', appHttpAdminOrderDetail::$repeater, appConfigPlan::repeater);
                            ?>
                            <?php
                            $btn = <<<EOF
                             <div data-disp="form-radio-repeater-2:checked" class="d-none">
                             <button type="button" class="btn btn-secondary" data-clientdata_load>ID登録</button>
                             </div>
                            EOF;
                            echo appLibraryForm::text('顧客ID', 'client_id', appHttpAdminOrderDetail::$client_id, $btn);
                            ?>
                            <?php
                            $parm = array('required' => true);
                            echo appLibraryForm::text('名前', 'name', appHttpAdminOrderDetail::$name, '', $parm);
                            ?>
                            <?php
                            $parm = array('required' => true);
                            echo appLibraryForm::text('名前（カナ）', 'name_kana', appHttpAdminOrderDetail::$name_kana, '', $parm);
                            ?>
                            <?php
                            $parm = array('required' => true);
                            echo appLibraryForm::text('電話番号', 'tel', appHttpAdminOrderDetail::$tel, '', $parm);
                            ?>
                            <?php
                            $parm = array('required' => true);
                            echo appLibraryForm::text('メール', 'mail', appHttpAdminOrderDetail::$mail, '', $parm);
                            ?>
                            <div class="row pt-3 pb-3 border-bottom">
                                <div class="col-4">住所</div>
                                <div class="col-8">
                                    <div class="d-none pb-3" data-toggle>
                                        <button type="button" class="btn btn-secondary" data-kojinAddressLoad><i class="fa fa-map-marker mr-2 color-lgray" aria-hidden="true"></i>ご遺体の安置場所と同じ</button>
                                    </div>
                                    <?php echo appLibraryForm::text('郵便番号<span class="font-small color-gray">（数字のみ）</span>', 'address1', appHttpAdminOrderDetail::$address[0]); ?>
                                    <?php
                                    $parm = array('required' => true);
                                    echo appLibraryForm::text('都道府県', 'address2', appHttpAdminOrderDetail::$address[1], '', $parm);
                                    ?>
                                    <?php
                                    $parm = array('required' => true);
                                    echo appLibraryForm::text('市区町村', 'address3', appHttpAdminOrderDetail::$address[2], '', $parm);
                                    ?>
                                    <?php
                                    $parm = array('required' => true);
                                    echo appLibraryForm::text('番地', 'address4', appHttpAdminOrderDetail::$address[3]);
                                    ?>
                                </div>
                            </div>
                            <?php echo appLibraryForm::text('故人様のお名前', 'kojin_name', appHttpAdminOrderDetail::$kojin_name); ?>
                            <?php echo appLibraryForm::text('故人様のお名前（カナ）', 'kojin_kana_name', appHttpAdminOrderDetail::$kojin_kana_name); ?>
                        </section>
                        <section class="pb-5">
                            <header class="pt-3 pb-4">
                                <h2 class="font-xxmiddle pb-3">葬儀会社の選択</h2>
                                <div class="d-none" data-toggle>
                                    <button type="button" class="btn btn-outline-secondary" data-companyselect='kojin'><i class="fa fa-map-marker mr-2 color-lgray" aria-hidden="true"></i>故人様の安置場所近辺</button>
                                    <button type="button" class="btn btn-outline-secondary" data-companyselect='client'><i class="fa fa-map-marker mr-2 color-lgray" aria-hidden="true"></i>お客様のご住所近辺</button>
                                    <button type="button" class="btn btn-outline-secondary" data-companyselect='default'>自由に選ぶ</button>
                                </div>
                            </header>
                            <?php
                            $btn = <<<EOF
                            <button type="button" class="btn btn-secondary d-none" data-companydata_load data-toggle>葬儀社IDを登録</button>
                            EOF;
                            echo appLibraryForm::text('葬儀社ID', 'company_id', appHttpAdminOrderDetail::$company_id, $btn);
                            ?>
                            <div class="pt-3">
                                <table class="table table-bordered">
                                    <tbody>
                                        <tr>
                                            <th class="bg-lgray w-25">社名</th>
                                            <td data-company-name><?php echo appHttpAdminOrderDetail::$company_name; ?></td>
                                        </tr>
                                        <tr>
                                            <th class="bg-lgray">住所</th>
                                            <td data-company-address><?php echo appHttpAdminOrderDetail::$company_address; ?></td>
                                        </tr>
                                        <tr>
                                            <th class="bg-lgray">電話番号</th>
                                            <td data-company-tel><?php echo appHttpAdminOrderDetail::$company_tel; ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <h2 class="font-xxmiddle pt-3 pb-3">葬儀式場の選択</h2>
                            <p>※まだ未設定</p>
                            <h2 class="font-xxmiddle pb-3">火葬場の選択</h2>
                            <p>※まだ未設定</p>
                        </section>

                        <section class="pb-5">
                            <header class="pt-3">
                                <h2 class="font-xxmiddle pb-2">その他：特筆事項</h2>
                                <p class="font-small">【自由記入欄】特別なご要望や、お客様の特徴（日本語が不自由・障害がある）など特筆する事があれば</p>
                            </header>
                            <?php echo appLibraryForm::textarea('', 'comment', appHttpAdminOrderDetail::$comment); ?>
                        </section>

                        <div class="pt-3 pb-5">
                            <input name="posttype" type="hidden" value="<?php echo appHttpAdminOrderDetail::$posttype; ?>">
                            <input name="id" type="hidden" value="<?php echo appHttpAdminOrderDetail::$id; ?>">
                            <input name="saijo_id" type="hidden" value="<?php echo appHttpAdminOrderDetail::$saijo_id; ?>">
                            <input name="kasou_id" type="hidden" value="<?php echo appHttpAdminOrderDetail::$kasou_id; ?>">
                            <button id="btn-submit" type="button" class="btn btn-lg btn-primary mx-auto d-none" data-toggle>更新</button>
                        </div>
                    </form>
                </section>
            </div>
            <div id="sec-search" class="col-4 p-3">
                <div class="container"></div>
            </div>
        </div>
    </div>
</article>
<script>
    $(function() {
        console.log("javascript:working");
        /*ラジオボタン押下：新規顧客or既存顧客*/
        $('[name=repeater]').click(function() {
            let src = '/admin/client';
            return formRadioRepeater(src);
        });
        /*入力フォーム変更：ご遺体の安置場所>郵便番号*/
        $('[name=kojin_address1]').change(function() {
            let postal_code = $(this).val();
            let area = '[name=kojin_address2]';
            let city = '[name=kojin_address3]';
            let banti = '[name=kojin_address4]';
            return getAddress(postal_code, area, city, banti);
        });
        /*入力フォーム変更：参列者数*/
        $('[name=int_ninzu]').change(function() {
            let ninzu = $(this).val();
            $('[name=option_food_tuya]').val(ninzu);
            $('[name=option_food_shoujin]').val(ninzu);
            $('[name=option_henreihin]').val(ninzu);
        });
        /*入力フォーム変更：個人情報>住所*/
        $('[name=address1]').change(function() {
            let postal_code = $(this).val();
            let area = '[name=address2]';
            let city = '[name=address3]';
            let banti = '[name=address4]';
            return getAddress(postal_code, area, city, banti);
        });
        /*ボタン押下：住所読み込み*/
        $('[data-kojinAddressLoad]').click(function() {
            let kojin_address1 = $('[name=kojin_address1]').val();
            let kojin_address2 = $('[name=kojin_address2]').val();
            let kojin_address3 = $('[name=kojin_address3]').val();
            let kojin_address4 = $('[name=kojin_address4]').val();
            $('[name=address1]').val('').val(kojin_address1);
            $('[name=address2]').val('').val(kojin_address2);
            $('[name=address3]').val('').val(kojin_address3);
            $('[name=address4]').val('').val(kojin_address4);
        });
        /*ボタン押下：葬儀社選択*/
        $('[data-companyselect]').click(function() {
            let url = '/admin/company';
            let type = $(this).data('companyselect');
            let area = '';
            let city = '';
            let src = '';
            switch (type) {
                case 'client':
                    /*喪主様の住所を基点*/
                    area = $('[name=address2]').val();
                    city = $('[name=address3]').val();
                    break;
                case 'kojin':
                    /*故人様の住所を基点*/
                    area = $('[name=kojin_address2]').val();
                    city = $('[name=kojin_address3]').val();
                    break;
            }
            src = url + '?area=' + area + '&city=' + city + '&posttype=area';
            changeIframe(src);
        });
        /*入力フォーム変更：顧客データ読み込み*/
        $('[data-client_id]').change(function() {
            let client_id = $(this).val();
            clientdataLoad(client_id);
        });
        /*入力フォーム変更：会社データ読み込み*/
        $('[name=company_id]').change(function() {
            let company_id = $(this).val();
            companydataLoad(company_id);
        });
        /*登録内容変更ボタン押下*/
        $('#btn-update').click(function() {
            let dataToggleLength = $('[data-toggle]').length;
            for (let i = 0; i <= dataToggleLength; i++) {
                if ($('[data-toggle]').eq(i).hasClass('d-none')) {
                    $('[data-toggle]').eq(i).removeClass('d-none');
                } else {
                    $('[data-toggle]').eq(i).addClass('d-none');
                }
            }
        });
        /*送信ボタン押下*/
        $('#btn-submit').click(function() {
            formSubmit('#formarea');
        });
        /*
        関数：AJAX
        */
        function ajaxload(url, params) {
            return $.ajax({
                type: "GET",
                url: url + params,
                dataType: "html"
            })
        }
        /*
        関数：郵便番号から住所取得
        */
        function getAddress(postal_code, area, city, banti) {
            ajaxload("/admin/ajax/address?postal_code=", postal_code).done(function(data) {
                    data = JSON.parse(data);
                    if (data.length > 0) {
                        $(area).val(data[0][1]);
                        $(city).val(data[0][2]);
                        $(banti).val('');
                    } else {
                        alert('存在しない郵便番号か、まだ登録されていない郵便番号です。後者の場合、都道府県と番地は手入力してください。')
                    }
                })
                // Ajaxリクエストが失敗した場合
                .fail(function(XMLHttpRequest, textStatus, errorThrown) {
                    $(area).val('');
                    $(city).val('');
                });
        }
        /*
        関数：顧客情報読み込み
        */
        function clientdataLoad(client_id) {
            ajaxload("/admin/ajax/client?id=", postal_code).done(function(data) {
                    data = JSON.parse(data);
                    console.log(data);
                    if (data.length == 0) {
                        $('[name="name"]').val('');
                        $('[name=name_kana]').val('');
                        $('[name=tel]').val('');
                        $('[name=address1]').val();
                        $('[name=address2]').val();
                        $('[name=address3]').val();
                        $('[name=address4]').val();
                        $('[name=mail]').val('');
                        alert('存在しない顧客IDです');
                    } else {
                        $('[name="name"]').val(data[0]['name']);
                        $('[name=name_kana]').val(data[0]['name_kana']);
                        $('[name=tel]').val(data[0]['tel']);
                        $('[name=address1]').val(data[0]['address1']);
                        $('[name=address2]').val(data[0]['address2']);
                        $('[name=address3]').val(data[0]['address3']);
                        $('[name=address4]').val(data[0]['address4']);
                        $('[name=mail]').val(data[0]['mail']);
                    }
                    return true;
                })
                // Ajaxリクエストが失敗した場合
                .fail(function(XMLHttpRequest, textStatus, errorThrown) {
                    alert('エラーが発生しました');
                    return false;
                });
        }
        /*
        関数：葬儀会社データ読み込み
        */
        function companydataLoad(company_id) {
            ajaxload("/admin/ajax/company?id=", company_id).done(function(data) {
                    data = JSON.parse(data);
                    if (data.length > 0) {
                        $('[data-company-name]').empty().append(data[0]['name']);
                        $('[data-company-address]').empty().append(data[0]['tel']);
                        $('[data-company-tel]').empty().append(data[0]['address']);
                    } else {
                        alert('存在しない葬儀社IDです');
                        $('[data-company-name]').empty();
                        $('[data-company-address]').empty();
                        $('[data-company-tel]').empty();
                    }
                })
                // Ajaxリクエストが失敗した場合
                .fail(function(XMLHttpRequest, textStatus, errorThrown) {
                    alert('通信に失敗しました。もう一度やり直してみてください');
                });
        }
        /*
        関数：iframe切り替え
        */
        function changeIframe(src) {
            let iframe = '';
            if (src != null) {
                iframe = '<iframe class="detailpage-iframe" src="' + src + '"></iframe>';
            }
            $('#sec-search').empty().append(iframe);
        }
        /*
        関数：個人情報（新規orリピーター）のラジオボタン
        */
        function formRadioRepeater(src = null) {
            let id = $('[name=repeater]:checked').attr('id');
            switch (id) {
                case 'form-radio-repeater-1':
                    $('[data-disp="form-radio-repeater-2:checked"]').addClass('d-none');
                    $('[name=client_id]').val('').attr('readonly', true);
                    break;
                case 'form-radio-repeater-2':
                    $('[data-disp="form-radio-repeater-2:checked"]').removeClass('d-none');
                    $('[name=client_id]').val('').attr('readonly', false);
                    break;
            }
            if (src != null) {
                return changeIframe(src);
            }
        }
        /*
        関数：SUBMIT
        */
        function formSubmit(targetId) {
            let repeater = $('[name=repeater]:checked').val();
            let errormsg_noRequired = '必須項目が未記入です';
            let errormsg_clientidNull = '項目「お客様ID」を確認してください。値が入力されていません';
            let errormsg_clientidFalse = '項目「お客様ID」を確認してください。入力されたお客様IDは存在しません';
            let errormsg_clientidInsert = '項目「お客様ID」を確認してください。新規のお客様の場合、お客様IDを入力しないでください（IDは新規発行されます）';
            let errormsg_unexpected = '想定外のエラーが発生しました。入力した内容はメモか何かに取った上で、ページをリロードしてください。';
            let errormsg_dbError = '通信エラーが発生しました。再度送信ボタンを押してみてください。';
            let id = $('[name=client_id]').val();
            /*必須項目が記入されているか*/
            if (!$(targetId)[0].reportValidity()) {
                return alert(errormsg_noRequired);
            }
            if (repeater == 1) {
                /*お客様がリピーターの場合・お客様IDは記入されているか？*/
                if (id == null || id == '') {
                    return alert(errormsg_clientidNull);
                }
                /*お客様がリピーターの場合・入力されたお客様IDは存在するか？*/
                ajaxload("/admin/ajax/company?id=", id).done(function(data) {
                        data = JSON.parse(data);
                        console.log(data);
                        if (data.length == 0) {
                            //IDが存在しない
                            return alert(errormsg_clientidFalse);
                        } else {
                            //IDが存在
                            $(targetId).submit();
                        }
                    })
                    .fail(function(XMLHttpRequest, textStatus, errorThrown) {
                        //DBの返答エラー
                        return alert(errormsg_unexpected);
                    });
            } else if (repeater == 0) {
                /*お客様が新規の場合・お客様IDは空白か？（ID入力されてても送信されることはないが一応）*/
                <?php if (appHttpAdminOrderDetail::$posttype == "insert") : ?>
                    if (id.length > 0) {
                        return alert(errormsg_clientidInsert);
                    } else {
                        $(targetId).submit();
                    }
                <?php else : ?>
                    $(targetId).submit();
                <?php endif; ?>
            } else {
                /*お客様が新規でもリピーターでもない場合（ありえない事象）*/
                alert(errormsg_unexpected);
            }
        }
        <?php if (appHttpAdminOrderDetail::$posttype == "insert") : ?>
            $('#btn-update').trigger("click");
        <?php endif; ?>
        changeIframe('/admin/company/?area=&city=&posttype=area');
    });
</script>
<?php
include_once '../../_template/l_footer.php';
?>