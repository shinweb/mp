<?php
include_once '../../_app/http/admin/order/index.php';
appConfig::$tmpl = 'index';
appConfig::$title = '注文情報一覧';
include_once '../../_template/admin/head.php';
include_once '../../_template/admin/l_header.php';
?>

<article>
    <div class="container pb-4">
        <div class="row pb-3 border-bottom mb-4">
            <div class="col-6">
                <a href="/admin/">TOP</a>
                <h1 class="pb-2"><?php echo appConfig::$title; ?></h1>
            </div>
            <div class="col-6 text-right"><a href="./detail" class="btn btn-lg btn-primary">データの新規追加</a></div>
        </div>
        <form method="get" class="pb-4">
            <div class="row pb-4">
                <div class="col-2">並び順：</div>
                <div class="col-6">
                    <?php echo appLibraryForm::selectmenu_def('sort', appHttpAdminOrderIndex::$get_sort, appConfigForm::sort); ?>
                </div>
            </div>
            <div class="row pb-3">
                <div class="col-2">ステータス：</div>
                <div class="col-9">
                    <div class="form-check form-check-inline pb-3">
                        <?php if (appHttpAdminOrderIndex::$get_status === 'all') : ?>
                            <input type="radio" class="form-check-input" id="form-radio-status-all" name="status" value="all" checked>
                        <?php else : ?>
                            <input type="radio" class="form-check-input" id="form-radio-status-all" name="status" value="all">
                        <?php endif; ?>
                        <label class="form-check-label mr-2" for="form-radio-status-all">全て表示</label>
                    </div>
                    <?php echo appLibraryForm::radio('status', appHttpAdminOrderIndex::$get_status, appConfigPlan::status); ?>
                </div>
            </div>
            <div class="row pb-4">
                <div class="col-2">フリーワード検索：</div>
                <div class="col-6">
                    <input type="text" class="form-control w-100" name="words" value="<?php echo appHttpAdminOrderIndex::$get_words; ?>">
                    <div class="pt-3"><small>お客様の氏名、住所、電話番号、メールアドレス／故人の氏名と住所／葬儀会社の名称と住所／各注文のコメント欄を対象に検索します</small></div>
                </div>
            </div>
            <button type="submit" class="btn btn-primary">検索</button>
        </form>
        <p><?php echo appHttpAdminOrderIndex::$msg; ?></p>
        <table class="table table-bordered">
            <tr class="bg-lgray">
                <th class="text-center">ID</th>
                <th class="text-center">喪主名</th>
                <th class="text-center">ステータス</th>
                <th class="text-center">希望日</th>
                <th class="text-center">施工日</th>
                <th class="text-center">登録日／更新日</th>
                <th></th>
            </tr>
            <?php echo appHttpAdminOrderIndex::list(appHttpAdminOrderIndex::$results_order, appHttpAdminOrderIndex::$results_client); ?>
        </table>
        <div class="p-4">
            <?php echo appLibraryNav::pager(appHttpAdminOrderIndex::$pageCount, 10); ?>
        </div>
    </div>
</article>

<?php
include_once '../../_template/l_footer.php';
?>