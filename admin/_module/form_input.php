<div class="row align-items-center pb-2 pt-2 border-bottom">
    <div class="col-4">
        <?php echo $title; ?>
        <?php if ($badge === true) : ?>
            <span class="badge badge-danger ml-2">必須</span>
        <?php endif; ?>
    </div>
    <?php if ($addCol === true) : ?>
        <div class="col-2">
            <span data-toggle><?php echo $value; ?></span>
            <input type="<?php echo $inputtype; ?>" class="form-control d-none" name="<?php echo $name; ?>" value="<?php echo $value; ?>" data-toggle<?php echo $addParm; ?>>
        </div>
    <?php else : ?>
        <div class="col-8">
            <span data-toggle><?php echo $value; ?></span>
            <input type="<?php echo $inputtype; ?>" class="form-control d-none" name="<?php echo $name; ?>" value="<?php echo $value; ?>" data-toggle<?php echo $addParm; ?>>
        </div>
    <?php endif; ?>
    <?php if ($addCol === true) : ?>
        <div class="col-4"><?php echo $addText; ?></div>
    <?php endif; ?>
</div>