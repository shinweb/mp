<?php
include_once '../../_app/http/admin/news/index.php';
appConfig::$tmp = 'admin';
appConfig::$title = appConfig::adminCategory['news']['name'];
appConfig::$css = '<link href="/asset/css/admin.css?' . appConfig::$update . '" rel="stylesheet" />';
include_once '../../_template/admin/head.php';
include_once '../../_template/admin/l_header_admin.php';
?>

<article class="h-100per">
    <?php appViewsHeading::h1Text(appConfig::$title, '<a href="http://osoushiki-tokyo.jp/#news" target="_blank">http://osoushiki-tokyo.jp/#news</a>'); ?>
    <div class="container pdt-middle">
        <p>トップページに掲載されている「お知らせ」を更新します。<br>ウェブサイトに新しいコンテンツが追加された時や告知したい情報がある場合。この画面から更新を行います。</p>
        <p><a href="./edit" class="btn btn-primary btn-lg w-200px d-block">新規追加</a></p>
    </div>
    <div class="container h-m90vh">
        <?php appViewsAdmin::list(appHttpAdminNews::$results); ?>
    </div>
</article>

<?php
include_once '../../_template/l_footer_admin.php';
?>