<?php
include_once '../../_app/http/remind/remind/index.php';
appConfig::$title = 'ID・パスワードの再設定';
include_once '../../_template/head.php';
include_once '../../_template/l_header.php';
?>
<h1 class="pt-5 text-center"><?php echo appConfig::$title; ?></h1>
<p class="text-center">記入したメールアドレス宛に<br>パスワード再発行用の URL を送付しました</p>
<?php
include_once '../../_template/l_footer.php';
?>