<?php
include_once '../../_app/http/remind/remind/confirm.php';
appConfig::$title = 'ID・パスワードの再設定';
include_once '../../_template/head.php';
include_once '../../_template/l_header.php';
?>
<h1 class="text-center pb-2 font-weight-bold"><?php echo appConfig::$title; ?></h1>
<div class="container text-center">
    <?php echo $body; ?>
</div>
<?php include_once '../../_template/l_footer.php'; ?>