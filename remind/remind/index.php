<?php
include_once '../../_app/http/remind/remind/index.php';
appConfig::$title = 'ID・パスワードの再設定';
include_once '../../_template/head.php';
include_once '../../_template/l_header.php';
?>

<?php
$title = appConfig::$title;
$comment = "ID・パスワードを再設定するためのメールを送信します";
$errMsg = "";
include_once '../../_module/form_remindmail.php';
?>

<?php
include_once '../../_template/l_footer.php';
?>