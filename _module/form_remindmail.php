<div class="bg-lgray">
    <div class="w-800px mx-auto">
        <header class="pb-3">
            <h1 class="text-center pb-2 font-weight-bold"><?php echo $title; ?></h1>
            <p class="text-center"><?php echo $comment; ?></p>
        </header>
        <div class="bg-white border p-4">
            <form method="post" action="./confirm">
                <div class="form-group">
                    <label class="font-size-middle-l font-weight-bold">ご登録メールアドレスを入力してください</label>
                    <input type="text" name="mail" class="form-control" value="" maxlength="40">
                </div>
                <div class="pt-3 pb-3">
                    <button type="submit" class="btn btn-primary btn-lg btn-block w-50 mx-auto">送信する</button>
                </div>
            </form>
        </div>
    </div>
</div>