<div class="bg-lgray">
    <div class="w-800px mx-auto">
        <h1 class="text-center pb-4 font-weight-bold"><?php echo $title; ?></h1>
        <?php echo $errMsg; ?>
        <div class="bg-white border p-4">
            <form method="post">
                <div class="form-group">
                    <label class="font-size-middle-l font-weight-bold">ログインID</label>
                    <input type="text" name="id" class="form-control" value="" maxlength="40">
                </div>
                <div class="form-group">
                    <label class="font-size-middle-l font-weight-bold">パスワード</label>
                    <input type="password" name="password" class="form-control" value="" maxlength="20">
                </div>
                <div class="pt-3 pb-3">
                    <input type="hidden" name="login">
                    <button type="submit" class="btn btn-primary btn-lg btn-block w-50 mx-auto">ログイン</button>
                    <?php echo $appendHtml; ?>
                </div>
            </form>
        </div>
    </div>
</div>